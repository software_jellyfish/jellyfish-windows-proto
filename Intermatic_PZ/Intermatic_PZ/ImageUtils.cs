﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace Intermatic_PZ
{
    class ImageUtils
    {
        public static Point PixelCoordsToImageControl(Image image, Point pixelpts, double imgCtrlActualWidth, double imgCtrlActualHeight)
        {           
           Size renderSize = new Size(imgCtrlActualWidth, imgCtrlActualHeight);
           Size sourceSize = new Size(((BitmapImage)image.Source).PixelWidth, ((BitmapImage)image.Source).PixelHeight);

           double xZoom = renderSize.Width / sourceSize.Width;
           double yZoom = renderSize.Height / sourceSize.Height;
           Point scale = DecomposeMatrixScale(((MatrixTransform)image.RenderTransform).Value);
           return new Point(pixelpts.X * xZoom * scale.X, pixelpts.Y * yZoom * scale.Y);

        }
        public static Point ImgControlCoordsToPixelCoords(Image image, Point locInCtrl, double imgCtrlActualWidth, double imgCtrlActualHeight)
        {
            if (image.Stretch == Stretch.None)
                return locInCtrl;

            Size renderSize = new Size(imgCtrlActualWidth, imgCtrlActualHeight);
            Size sourceSize = new Size(((BitmapImage)image.Source).PixelWidth, ((BitmapImage)image.Source).PixelHeight);

            double xZoom = renderSize.Width / sourceSize.Width;
            double yZoom = renderSize.Height / sourceSize.Height;

            if (image.Stretch == Stretch.Fill)
                return new Point(locInCtrl.X / xZoom, locInCtrl.Y / yZoom);

            double zoom;
            if (image.Stretch == Stretch.Uniform)
                zoom = Math.Min(xZoom, yZoom);
            else // (imageCtrl.Stretch == Stretch.UniformToFill)
                zoom = Math.Max(xZoom, yZoom);

            return new Point(locInCtrl.X / zoom, locInCtrl.Y / zoom);
        }
        public static Rect GetDisplayRect(FrameworkElement child, FrameworkElement parent, FrameworkElement grandparent)
        {
            Matrix m = ((MatrixTransform)child.RenderTransform).Value;
            Point relativeLocation = child.TranslatePoint(new Point(0, 0), parent);
            Point relativeChildLocation = parent.TranslatePoint(new Point(0, 0), child);
            //pixel = ImageUtils.ImgControlCoordsToPixelCoords(image, relativeLocation, image.ActualWidth, image.ActualHeight);
            // Transform screen point to WPF device independent point
            Point absLocation = parent.PointToScreen(relativeLocation);
            Point relativeLocation2 = parent.TranslatePoint(new Point(0, 0), (FrameworkElement)parent.Parent);
            //pixel = ImageUtils.ImgControlCoordsToPixelCoords(image, relativeLocation, image.ActualWidth, image.ActualHeight);
            // Transform screen point to WPF device independent point
            Point absLocation2 = grandparent.PointToScreen(relativeLocation2);

            
            return new Rect(0, 0, 0, 0);
        }
        public static void DecomposeMatrix(Matrix m, ref double rotation, ref Point scale, ref Point translation)
        {
            if ((m.M12 != 0) || (m.M21 != 0))
                throw new NotImplementedException("Rotation not implemented");
            else
            {
                rotation = 0;
                scale.X = m.M11;
                scale.Y = m.M22;
                translation.X = m.OffsetX;
                translation.Y = m.OffsetY;
            }
        }
        public static Point DecomposeMatrixTranslation(Matrix m)
        {
            Point translation = new Point(m.OffsetX, m.OffsetY);
            return translation;
        }
        public static Point DecomposeMatrixScale(Matrix m)
        {
            if ((m.M12 != 0) || (m.M21 != 0))
                throw new NotImplementedException("Rotation not implemented");
            else
            {
                Point scale = new Point(m.M11, m.M22);
                return scale;
            }
        }
    }
}
