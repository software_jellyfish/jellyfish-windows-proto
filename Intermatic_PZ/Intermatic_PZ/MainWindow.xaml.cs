﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Diagnostics;

using log4net;
namespace Intermatic_PZ
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point _origin;
        private Point _start;
        private BitmapImage _bitmap = null;
        private string[] _filelist = new string[5];
        private int _current_index = -1;
        private int _count = 0;
        private Dictionary<int, Chiclet> _chiclets = new Dictionary<int, Chiclet>();
        private int _chicletID = 0;
        private bool _addChicletMode = false;
        private static double NUM_TESTS = Math.Sqrt(100)+1;
       
        public MainWindow()
        {
            log4net.Config.XmlConfigurator.Configure();
            App._LOGGER = log4net.LogManager.GetLogger(typeof(log4net.Appender.RollingFileAppender));
            App._LOGGER.Debug("Hello!");

            /*App._LOGGLY = log4net.LogManager.GetLogger(typeof(log4net.loggly.LogglyAppender));
            App._LOGGLY.Debug("Hello!");*/
            var logger = LogManager.GetLogger(typeof(log4net.loggly.LogglyAppender));
            //Send plaintext
            logger.Info("your log message");

            //Send an exception
            logger.Error("your log message", new Exception("your exception message"));

            //Send a JSON object
            var items = new Dictionary<string, string>();
            items.Add("key1", "value1");
            items.Add("key2", "value2");
            logger.Info(items);

            InitializeComponent();
            _filelist[0] = @"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor1.png";
            _filelist[1] = @"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor2.png";
            _filelist[2] = @"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor3.png";
            _filelist[3] = @"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor4.png";
            _filelist[4] = @"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor5.png";
            image.Source = null;
            image.Source = BitmapFromUri(new Uri(@"C:\Users\ecolgan\Documents\Visual Studio 2013\Projects\jellyfish-windows-proto\Intermatic_PZ\Intermatic_PZ\Images\floorplans\floorPlan-dummyGraphPaper-150dpi-floor3.png", UriKind.Absolute));
            Cursor = Cursors.SizeAll;
            image.MouseWheel += image_MouseWheel;
            image.MouseLeftButtonDown += image_MouseLeftButtonDown;
            image.MouseLeftButtonUp += image_MouseLeftButtonUp;
            image.MouseMove += image_MouseMove;
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0,0,1);
            dispatcherTimer.Start();
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => ResetScale()));
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => addTestChiclets()));
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => MoveChiclets()));
                       
        }
        private void addTestChiclets()
        {
            Random r = new Random();
            if (NUM_TESTS > 0)
            {
                double space = 1 / NUM_TESTS;
                for (int i = 0; i < NUM_TESTS; i++)
                    for (int j = 0; j < NUM_TESTS; j++)
                    {
                        double rspacei = space * r.Next(90, 110)/100.0;
                        double rspacej = space * r.Next(90, 110) / 100.0;
                        addChiclet(++_chicletID, new Point(rspacei * i, rspacej * j));
                    }
                        
                addChiclet(++_chicletID, new Point(0.275, 0.35));
                addChiclet(++_chicletID, new Point(0.2625, 0.375));
            }
            SortChiclets();
        }
        private void ChicletChangedMsg(Chiclet obj, string data)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => ChicletUpdate(obj.ID)));
        }
        private void ChicletUpdate(int id)
        {
            if(_chiclets.ContainsKey(id))
                _chiclets[id].UpdateBorder();
        }
        private void addChiclet(int id, Point pos)
        {
            Chiclet c = new Chiclet(image, id, ChicletChangedMsg);
            c.Position = new Point(pos.X, pos.Y);
            canvas.Children.Add(c.UIelement);
            c.UIelement.MouseMove += image_MouseMove;
            c.UIelement.MouseWheel += image_MouseWheel;            
            _chiclets.Add(c.ID, c);
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if(_current_index >= 0)
            {
                image.Source = null;
                image.Source = BitmapFromUri(new Uri(_filelist[_current_index], UriKind.Absolute));
                ResetScale();
                _count++;
                this.Title = String.Format("{0}, loads = {1}",_filelist[_current_index],  _count);
                App._LOGGER.Info("Loaded new File: "+ _filelist[_current_index]);
                _current_index++;
                if (_current_index >= _filelist.Length)
                    _current_index = 0;
                RemoveChiclets();
                addTestChiclets();
                MoveChiclets();
            }
            else
                MoveChiclets();
            long usedMemory = Process.GetCurrentProcess().WorkingSet64;
             usedMemory /= (1024 * 1024); //GC.GetTotalMemory(false) / (1024 ^ 2);
            memUsageText.Text = usedMemory.ToString() + "MB";
            

        }
        private void MoveChiclets()
        {
            foreach (KeyValuePair<int, Chiclet> c in _chiclets)
            {
                c.Value.UIelement.Visibility = Visibility.Visible;
                c.Value.Move(canvas);
            }
        }
        
        private void image_MouseMove(object sender, MouseEventArgs e)
        {
            Matrix m = ((MatrixTransform)image.RenderTransform).Value;
            Point pixel = ImageUtils.ImgControlCoordsToPixelCoords(image, e.GetPosition(image), image.ActualWidth, image.ActualHeight);
            pixelPosText.Text = String.Format("{0}, {1}", pixel.X.ToString("0"), pixel.Y.ToString("0"));
            Point relativeLocation = image.TranslatePoint(new Point(0, 0), imageHome);
            Point relativeChildLocation = imageHome.TranslatePoint(new Point(0, 0), image);
            // Transform screen point to WPF device independent point
            Point absLocation= imageHome.PointToScreen(relativeLocation);
            Point relativeLocation2 = imageHome.TranslatePoint(new Point(0, 0), LayoutRoot);
            // Transform screen point to WPF device independent point
            Point absLocation2 = LayoutRoot.PointToScreen(relativeLocation2);
           
            // Set coordinates
            chicletSizeText.Text = String.Format("{0}, {1}", (relativeLocation.X).ToString("0"), (relativeLocation.Y).ToString("0"));
            Console.WriteLine(String.Format("{0}, {1}, {2}, {3}", LayoutRoot.ActualWidth, LayoutRoot.ActualHeight, image.ActualWidth, image.ActualHeight));
            Console.WriteLine(String.Format("{0}, {1}, {2}, {3}, {4}, {5}", relativeLocation.X, image.ActualWidth * m.M11, relativeLocation.X + image.ActualWidth * m.M11, LayoutRoot.ActualWidth - relativeLocation.X, relativeChildLocation.X * m.M11, relativeChildLocation.Y));
            if (!image.IsMouseCaptured)
                return;            
            Vector v = _start - e.GetPosition(canvas);
            m.OffsetX = _origin.X - v.X;
            m.OffsetY = _origin.Y - v.Y;
            image.RenderTransform = new MatrixTransform(m);
            MoveChiclets();

        }
        private void SortChiclets()
        {
            /*_chiclets.Sort(
                delegate (Chiclet p1, Chiclet p2)
                {
                    int comparePos = p1.Position.X.CompareTo(p2.Position.X);
                    if (comparePos == 0)
                    {
                        return p1.Position.Y.CompareTo(p2.Position.Y);
                    }
                    return comparePos;
                }
            );*/
        }
        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(_addChicletMode)
            {
                Point p = ImageUtils.ImgControlCoordsToPixelCoords(image, e.GetPosition(image), image.ActualWidth, image.ActualHeight);
                Point s = new Point(((BitmapImage)image.Source).PixelWidth, ((BitmapImage)image.Source).PixelHeight);
                addChiclet(++_chicletID, new Point(p.X / s.X, p.Y/s.Y));                
                Cursor = Cursors.Arrow;
                _addChicletMode=false;
                SortChiclets();
                return;
            }
            
            image.CaptureMouse();
            Cursor = Cursors.ScrollAll;
            Matrix m = ((MatrixTransform)image.RenderTransform).Value;
            _start = e.GetPosition(canvas);
            _origin = new Point(m.OffsetX, m.OffsetY);
        }

        private void image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            image.ReleaseMouseCapture();
            Cursor = Cursors.Arrow;


            MoveChiclets();
        }
        private void image_zoom(int delta, Point center)
        {
            Matrix m = ((MatrixTransform)image.RenderTransform).Value;
            Point scala = ImageUtils.DecomposeMatrixScale(m);
            Point trans = ImageUtils.DecomposeMatrixTranslation(m);
            double adjustment = 0.01;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                adjustment *= 5;
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {

                adjustment *= 10;
            }
            if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
            {
                adjustment *= .5;
            }
            double zoom = delta > 0 ? adjustment : -adjustment;

            if ((scala.X + zoom) > 0.001)
            {
                Point relative = center;
                double abosuluteX;
                double abosuluteY;
                abosuluteX = relative.X * scala.X + trans.X;
                abosuluteY = relative.Y * scala.Y + trans.Y;
                scala.X += zoom;
                scala.Y += zoom;
                trans.X = abosuluteX - relative.X * scala.X;
                trans.Y = abosuluteY - relative.Y * scala.Y;
                image.RenderTransform = new MatrixTransform(new Matrix(scala.X, 0, 0, scala.Y, trans.X, trans.Y));
                image.UpdateLayout();
            }
        }
        private void image_MouseWheel(object sender, MouseWheelEventArgs e)
        {            
            image_zoom(e.Delta, e.GetPosition(image));
            MoveChiclets();
        }
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ResetScale();

        }
        private void ResetScale()
        {
            _origin.X = 0; _origin.Y = 0;
            _start.X = 0; _start.Y = 0;
           
            Matrix m = new Matrix(0.2, 0, 0, 0.2, 0, 0);
            image.RenderTransform = new MatrixTransform(m);
            image.UpdateLayout();
            foreach(KeyValuePair < int, Chiclet > c in _chiclets)
            {
                m = ((MatrixTransform)c.Value.UIelement.RenderTransform).Value;
                c.Value.UIelement.RenderTransform = new MatrixTransform(new Matrix(1, 0, 0, 1, m.OffsetX, m.OffsetY));
            }
            MoveChiclets();
        }

        private void Open_File_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    image.Source = null;
                    image.Source = BitmapFromUri(new Uri(openFileDialog.FileName, UriKind.Absolute));
                    ResetScale();
                    RemoveChiclets();
                    addTestChiclets();
                    MoveChiclets();
                    App._LOGGER.Info("Opened new File: " + openFileDialog.FileName);
                }
                catch(Exception exc)
                {
                    //
                    App._LOGGER.Error("Error Opening new file: " + openFileDialog.FileName);
                    App._LOGGER.Error(exc.Message);
                    App._LOGGER.Error(exc.StackTrace);
                    MessageBox.Show(exc.Message);
                }
            }

        }
        public  ImageSource BitmapFromUri(Uri source)
        {
            if (_bitmap != null)
            {
                _bitmap.Freeze();
                image.Source = null;
                _bitmap = null;
                GC.Collect();
            }
            image.Source = null;
            _bitmap = null;
            _bitmap = new BitmapImage();
            _bitmap.BeginInit();
            _bitmap.UriSource = source;
            _bitmap.CacheOption = BitmapCacheOption.OnLoad;
            _bitmap.EndInit();
            return (ImageSource)_bitmap;
        }

        private void Open_Files_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if(_current_index >= 0)
                _current_index = -1;
            else
                _current_index = 0;
        }

        private void border1_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            image_MouseWheel( sender,  e);
        }

        private void border1_MouseMove(object sender, MouseEventArgs e)
        {
            image_MouseMove(sender, e);
        }

        private void border1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void border1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void AddChiclet_Click(object sender, RoutedEventArgs e)
        {
            _addChicletMode = true;            
            Cursor = Cursors.Cross;
        }
        private void RemoveChiclets()
        {
            //First, remove chiclets from the visualization tree.
            foreach (KeyValuePair<int, Chiclet> c in _chiclets)
                canvas.Children.Remove(c.Value.UIelement);
            _chiclets.Clear();
            GC.Collect();
        }
        private void ClearChiclets_Click(object sender, RoutedEventArgs e)
        {
            RemoveChiclets();
        }

        private void border_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            e.ManipulationContainer = this;
            e.Handled = true;
        }

        private void border_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingEventArgs e)
        {
             // Decrease the velocity of the Rectangle's movement by 
            // 10 inches per second every second.
            // (10 inches * 96 pixels per inch / 1000ms^2)
            e.TranslationBehavior.DesiredDeceleration = 10.0 * 96.0 / (1000.0 * 1000.0);

            // Decrease the velocity of the Rectangle's resizing by 
            // 0.1 inches per second every second.
            // (0.1 inches * 96 pixels per inch / (1000ms^2)
            e.ExpansionBehavior.DesiredDeceleration = 0.1 * 96 / (1000.0 * 1000.0);

            // Decrease the velocity of the Rectangle's rotation rate by 
            // 2 rotations per second every second.
            // (2 * 360 degrees / (1000ms^2)
            e.RotationBehavior.DesiredDeceleration = 720 / (1000.0 * 1000.0);

            e.Handled = true;
        }

        private void border_ManipulationDelta(object sender, ManipulationDeltaEventArgs e)
        {
            // Get the Rectangle and its RenderTransform matrix.
            //Border rectToMove = e.OriginalSource as Border;
            Matrix rectsMatrix = ((MatrixTransform)image.RenderTransform).Matrix;

            // Rotate the Rectangle.
            /* rectsMatrix.RotateAt(e.DeltaManipulation.Rotation,
                                 e.ManipulationOrigin.X,
                                 e.ManipulationOrigin.Y);*/

            // Resize the Rectangle.  Keep it square 
            // so use only the X value of Scale.
            rectsMatrix.ScaleAt(e.DeltaManipulation.Scale.X,
                                e.DeltaManipulation.Scale.X,
                                e.ManipulationOrigin.X,
                                e.ManipulationOrigin.Y);

            // Move the Rectangle.
            rectsMatrix.Translate(e.DeltaManipulation.Translation.X,
                                  e.DeltaManipulation.Translation.Y);

            // Apply the changes to the Rectangle.
            //rectsMatrix.ScaleAt(e.DeltaManipulation.Scale.X, 1, e.ManipulationOrigin.X, e.ManipulationOrigin.Y);
            image.RenderTransform = new MatrixTransform(rectsMatrix);
            image.UpdateLayout();
            MoveChiclets(); 
            Rect containingRect =
                new Rect(((FrameworkElement)e.ManipulationContainer).RenderSize);

            Rect shapeBounds =
                canvas.RenderTransform.TransformBounds(
                    new Rect(canvas.RenderSize));

            // Check if the rectangle is completely in the window.
            // If it is not and intertia is occuring, stop the manipulation.
            if (e.IsInertial && !containingRect.Contains(shapeBounds))
            {
                e.Complete();
            }


            e.Handled = true;
            /*Roatation complicates things
             * [sx  0  0  tx]
             | 0 sy  0  ty|
             | 0  0 sz  tz|
             [ 0  0  0  1],
             but Matrix is 
             [sx 0
             0   sy], OffsetX, OffsetY
            
             * If we build the matrix with rotation , scale, translate -- 
             [cosA*sx    -sinA*sy    0
             * sinA*sx   cosA*sy     0
             * tx         ty           1]
             *
             */
            rectsMatrix = canvas.RenderTransform.Value;
        }

        private void border_PreviewMouseMove(object sender, MouseEventArgs e)
        {}
        private void image_ChicletMouseMove(MouseEventArgs e)
        {
            Point pixel = ImageUtils.ImgControlCoordsToPixelCoords(image, e.GetPosition(image), image.ActualWidth, image.ActualHeight);
            chicletSizeText.Text = String.Format("{0}, {1}", pixel.X.ToString("0"), pixel.Y.ToString("0"));
        }

        // Create a method for a delegate.
        public static void ChicletDelegateMethod(Chiclet obj, string message, MouseEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background,
                        new Action(() => ((MainWindow)Application.Current.MainWindow).image_ChicletMouseMove(e)));
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}