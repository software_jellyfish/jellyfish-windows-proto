﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Markup;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;

using System.Windows.Interop;

namespace Intermatic_PZ
{
    public delegate void ChicletMsg(Chiclet obj, string data);
    public class Chiclet
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int WM_LBUTTONDOWN = 0x0201;
        public const int HT_CAPTION = 0x2;        
        public const int MK_LBUTTON = 0x0001;
        public const int WM_MOUSEMOVE = 0x0200;



        private int _id = -1;
        private FrameworkElement _uielement = null;
        private Image _image = null;
        private Point _pos;
        private bool _input;
        private bool _output;
        private ChicletMsg _changeDelegate = null;
        public Chiclet(Image image, int id, ChicletMsg changeDelegate)
        {
            _changeDelegate = changeDelegate;
            _image = image;
            _id = id;
            _input = false;
            _output = false;
            Random r = new Random();
            if (r.Next(0, 100) < 25)
                _input = true;
            if (r.Next(0, 100) < 50)
                _output = true;
            _uielement = (FrameworkElement)Chiclet.CreateChicletUIElement(id);
            _uielement.MouseLeftButtonDown += Chiclet_Click;
            _uielement.MouseMove += Chiclet_MouseMove;
            _uielement.PreviewMouseMove += Chiclet_PreviewMouseMove;
            _uielement.MouseEnter += Chiclet_MouseEnter;
            _uielement.MouseLeave += Chiclet_MouseLeave;
            Rectangle inp = (Rectangle)_uielement.FindName("input");
            Rectangle outp = (Rectangle)_uielement.FindName("output");
            inp.MouseLeftButtonDown += ChicletInput_Click;
            outp.MouseLeftButtonDown += ChicletOutput_Click;

            Matrix m = Matrix.Identity;
            m.Scale(1, 1);
            m.Translate(0, 0);
            _uielement.RenderTransform = new MatrixTransform(m);
            ((Border)_uielement).BorderBrush = GetBorderBrush();
            inp.Fill = _input ? Brushes.Red : Brushes.Green;
            outp.Fill = _output ? Brushes.Red : Brushes.Green;
        }
        

        public void RemoveUI()
        {
            _uielement = null;
            _image = null;
        }
        public void UpdateBorder()
        {
            ((Border)_uielement).BorderBrush = GetBorderBrush();
        }
        public Rect BoundingRect
        {
            get { return UIelement.RenderTransform.TransformBounds(new Rect(0, 0, UIelement.Width, UIelement.Height)); }
        }
        public int ID
        {
            get { return _id; }
        }
        public FrameworkElement UIelement
        {
            get { return _uielement; }
        }
        public Point Position
        {
            get { return _pos; }
            set { _pos = value; }
        }
        public static Rect BoundsRelativeTo(FrameworkElement child, FrameworkElement parent)
        {
            GeneralTransform gt = child.TransformToAncestor(parent);
            return gt.TransformBounds(new Rect(0, 0, child.ActualWidth, child.ActualHeight));
        }
        public void Move(FrameworkElement relative)
        {
            Point newpos = new Point(Position.X * ((BitmapImage)_image.Source).PixelWidth, Position.Y * ((BitmapImage)_image.Source).PixelHeight);
            Point pixpoint = ImageUtils.PixelCoordsToImageControl(_image, newpos, _image.ActualWidth, _image.ActualHeight);
            Point relativePoint = _image.TransformToAncestor(relative).Transform(new Point(0, 0));
            Matrix mR = ((MatrixTransform)_image.RenderTransform).Value;
            Matrix m = ((MatrixTransform)UIelement.RenderTransform).Value;
            Matrix m2 = Matrix.Identity;
            m2.OffsetX = relativePoint.X + pixpoint.X;
            m2.OffsetY = relativePoint.Y + pixpoint.Y;
            m2.M11 = 1;
            m2.M22 = 1;

            m2.M11 = -4.167 * mR.M11 * mR.M11 + 6.25 * mR.M11 - (0.5 / 6);//1 is a constant size that is positioned properly
            if (mR.M11 > 1.25)
                m2.M11 = 1.25;
            if (m2.M11 > 1.25)
                m2.M11 = 1.25;
            m2.M22 = m2.M11;// 5 * mR.M22;//1 is a constant size that is positioned properly

            UIelement.RenderTransform = new MatrixTransform(m2);
        }
        private Brush GetBorderBrush()
        {
            if (_input && !_output)
            {
                return Brushes.LimeGreen;
            }
            else if (!_input && _output)
            {
                return Brushes.DarkViolet;
            }
            else if (_input && _output)
            {
                return Brushes.Yellow;
            }
            else
                return Brushes.Blue;
        }
        private void ChicletInput_Click(object sender, RoutedEventArgs e)
        {
            Rectangle inp = (Rectangle)_uielement.FindName("input");
            _input = !_input;
            inp.Fill = _input ? Brushes.Red : Brushes.Green;
            //update the border from the outside
            if (this._changeDelegate != null)
                _changeDelegate(this, "");
        }
        private void ChicletOutput_Click(object sender, RoutedEventArgs e)
        {
            Rectangle outp = (Rectangle)_uielement.FindName("output");
            _output = !_output;
            outp.Fill = _output ? Brushes.Red : Brushes.Green;
            //update the border from the outside
            if (this._changeDelegate != null)
                _changeDelegate(this, "");
        }
        private void Chiclet_Click(object sender, MouseButtonEventArgs e)
        {
            HwndSource hwnd = (HwndSource)HwndSource.FromVisual(_image);
            Int32 lparm = (((int)e.GetPosition(_image).X) | ((int)e.GetPosition(_image).Y<<16));
           SendMessage(hwnd.Handle, WM_NCLBUTTONDOWN, MK_LBUTTON, lparm);
            Console.WriteLine("Clicked Chiclet " + ID.ToString());
        }
        private void Chiclet_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            HwndSource hwnd = (HwndSource)HwndSource.FromVisual(_image);
            Int32 lparm = (((int)e.GetPosition(_image).X) | ((int)e.GetPosition(_image).Y << 16));
            //SendMessage(hwnd.Handle, WM_MOUSEMOVE, 0, lparm);
            Console.WriteLine("Chiclet Mouse Move" + ID.ToString());
        }
        private void Chiclet_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Console.WriteLine("Chiclet Preview Mouse Move" + ID.ToString());
        }
        private void Chiclet_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _uielement.Cursor = System.Windows.Input.Cursors.Arrow;
            MainWindow.ChicletDelegateMethod(this, "Blah", e);
            ((Border)_uielement).BorderBrush = Brushes.Black;
            ((Border)_uielement).BorderThickness = new Thickness(7);
            System.Windows.Controls.Panel.SetZIndex(_uielement, 3);
        }
        private void Chiclet_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Console.WriteLine("Chiclet Tool Tip Open" + ID.ToString());
            //((Border)_uielement).BorderBrush = GetBorderBrush();
            ((Border)_uielement).BorderThickness = new Thickness(5);
            System.Windows.Controls.Panel.SetZIndex(_uielement, 2);
            //update the border from the outside
            if (this._changeDelegate != null)
                _changeDelegate(this, "");
        }

        private static string CreateChicletXaml(int id)
        {
            string ns = "xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"";
            string nsx = "xmlns:x=\"http://schemas.microsoft.com/winfx/2006/xaml\"";
            string baseformat = "<Border {0} {1} x:Name=\"{2}\" Panel.ZIndex=\"2\" BorderThickness=\"5\" BorderBrush=\"Blue\" Background=\"Transparent\" HorizontalAlignment=\"Left\" VerticalAlignment=\"Top\"     Width=\"60\"    Height=\"60\" >";
            baseformat += "<Grid>";
            baseformat += "<Grid.RowDefinitions>";
            baseformat += "<RowDefinition Height=\"30*\"/>";
            baseformat += "<RowDefinition Height=\"40*\"/>";
            baseformat += "<RowDefinition Height=\"20*\"/>";
            baseformat += "<RowDefinition Height=\"20*\"/>";
            baseformat += "</Grid.RowDefinitions>";
            baseformat += "<TextBlock TextAlignment=\"Center\" Grid.Row=\"0\"  VerticalAlignment=\"Top\" Background=\"Blue\" Foreground=\"White\"  FontSize=\"10\" FontWeight=\"Bold\" TextDecorations=\"Underline\">";
            baseformat += "Zone {3}";
            baseformat += "</TextBlock>";
            baseformat += "<Image x:Name=\"icon\" Grid.Row=\"1\" Source=\"Images\\ic_lightbulb_outline_black_24dp_1x.png\"></Image>";
            baseformat += "<Rectangle x:Name=\"input\" Grid.Row=\"2\" Fill=\"Red\" ></Rectangle >";
            baseformat += "<Rectangle x:Name=\"output\" Grid.Row=\"3\" Fill=\"Green\" ></Rectangle >";
            baseformat += "</Grid>";
            baseformat += "</Border>";
            return String.Format(baseformat, ns, nsx, "chiclet"+id.ToString(), id);
        }
        public static object CreateChicletUIElement(int id)
        {
            string xamlchiclet =  CreateChicletXaml(id);
            return XamlReader.Parse(xamlchiclet);
           
        }
        [DllImportAttribute("User32.dll")]
        //private static extern IntPtr SendMessage(int hWnd, int Msg, bool wParam, int lParam);
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, UInt32 wParam, Int32 lParam);

    }
}
