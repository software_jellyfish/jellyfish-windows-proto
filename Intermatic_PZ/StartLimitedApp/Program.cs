﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Reflection;
namespace StartLimitedApp
{
    class Program
    {
        /// <summary>
        /// Gets the last error throw.
        /// </summary>
        public static void GetLastErrorThrow()
        {
            //throw new Win32Exception(GetLastError());
        }
        public static string AssemblyDirectory
        {
            get
            {
                string path = AssemblyFullPath;
                return System.IO.Path.GetDirectoryName(path) + "\\";
            }
        }
        public static string AssemblyFullPath
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                return Uri.UnescapeDataString(uri.Path);
            }
        }
        static void Main(string[] args)
        {
            using (Process p = new Process())
            {
                Environment.CurrentDirectory = AssemblyDirectory;
                p.StartInfo = new ProcessStartInfo("Intermatic_PZ.exe");
                p.Start();
                using (Job j = Job.CreateJobWithMemoryLimits(
                    (uint)(uint)1024 * 1024 * 10,
                    (uint)1024*1024 * 1000,
                    p))
                {
                    // As long as the Job object j is alive, there
                    // will be a memory limit of 30 Mb on the Process p
                    Console.ReadLine();
                }
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    [CLSCompliant(false)]
    public class Job : IDisposable
    {
        private string m_name;
        private IntPtr? m_handle;
        private bool m_inheritSecurityHandle;
        private IntPtr m_securityDescriptor;

        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public Job(string name)
            : this(name, true, (IntPtr)null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Job"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="inheritSecurityHandle">if set to <c>true</c> [inherit security handle].</param>
        /// <param name="securityDescriptor">The security descriptor.</param>
        public Job(string name, bool inheritSecurityHandle, IntPtr securityDescriptor)
        {
            m_name = name;
            m_inheritSecurityHandle = inheritSecurityHandle;
            m_securityDescriptor = securityDescriptor;
            Create();
        }

        /// <summary>
        /// Creates the job with memory limits.
        /// </summary>
        /// <param name="minWorkingSetSize">Size of the min working set.</param>
        /// <param name="maxWorkingSetSize">Size of the max working set.</param>
        /// <param name="processesToLimit">The processes to limit.</param>
        /// <returns></returns>
        public static Job CreateJobWithMemoryLimits(uint minWorkingSetSize, uint maxWorkingSetSize, params Process[] processesToLimit)
        {
            Job job = new Job(StringUtilities.RandomString(10, true));
            job.SetLimitWorkingSetSize(minWorkingSetSize, maxWorkingSetSize);
            if (processesToLimit != null)
            {
                foreach (Process p in processesToLimit)
                {
                    job.AssignProcess(p);
                }
            }
            return job;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return m_name;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [inherit security handle].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [inherit security handle]; otherwise, <c>false</c>.
        /// </value>
        public bool InheritSecurityHandle
        {
            get
            {
                return m_inheritSecurityHandle;
            }
        }

        /// <summary>
        /// Gets the security descriptor.
        /// </summary>
        /// <value>The security descriptor.</value>
        public IntPtr SecurityDescriptor
        {
            get
            {
                return m_securityDescriptor;
            }
        }

        /// <summary>
        /// Releases unmanaged resources and performs other cleanup operations before the
        /// <see cref="Job"/> is reclaimed by garbage collection.
        /// </summary>
        ~Job()
        {
            Dispose();
        }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        public void Create()
        {
            if (m_handle != null && m_handle.Value != null)
            {
                throw new Exception("Previous handle exists");
            }

            Win32Structures.SECURITY_ATTRIBUTES securityAttributes = new Win32Structures.SECURITY_ATTRIBUTES();

            securityAttributes.bInheritHandle = InheritSecurityHandle ? 1 : 0;
            securityAttributes.lpSecurityDescriptor = SecurityDescriptor;
            securityAttributes.nLength = Marshal.SizeOf(securityAttributes);

            m_handle = ExternalMethods.CreateJobObject(ref securityAttributes, Name);
            if (m_handle == null || m_handle.Value == null)
            {
                ////GetLastErrorThrow();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (m_handle != null && m_handle.Value != null)
            {
                bool result = ExternalMethods.CloseHandle(m_handle.Value);

                m_handle = null;

                if (!result)
                {
                    //GetLastErrorThrow();
                }
            }
        }

        /// <summary>
        /// Sets the size of the limit maximum working set.
        /// </summary>
        /// <param name="minWorkingSetSize">Size of the min working set.</param>
        /// <param name="maxWorkingSetSize">Size of the max working set.</param>
        public void SetLimitWorkingSetSize(uint minWorkingSetSize, uint maxWorkingSetSize)
        {
            Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION limitInfo = new Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION();

            limitInfo.LimitFlags = Win32Enums.LimitFlags.JOB_OBJECT_LIMIT_WORKINGSET;
            limitInfo.MinimumWorkingSetSize = minWorkingSetSize;
            limitInfo.MaximumWorkingSetSize = maxWorkingSetSize;

            int size = Marshal.SizeOf(limitInfo);
            if (!ExternalMethods.SetInformationJobObjectLimit(
                m_handle.Value,
                Win32Enums.JobObjectInfoClass.JobObjectBasicLimitInformation,
                ref limitInfo,
                size
            ))
            {
                //GetLastErrorThrow();
            }
        }

        /// <summary>
        /// Queries the information job object limit.
        /// </summary>
        public Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION QueryInformationJobObjectLimit()
        {
            Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION limitInfo = new Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION();

            int size;
            if (!ExternalMethods.QueryInformationJobObjectLimit(
                m_handle.Value,
                Win32Enums.JobObjectInfoClass.JobObjectBasicLimitInformation,
                out limitInfo,
                Marshal.SizeOf(limitInfo),
                out size
            ))
            {
                //GetLastErrorThrow();
            }

            return limitInfo;
        }

        /// <summary>
        /// Assigns the process.
        /// </summary>
        /// <param name="process">The process.</param>
        public void AssignProcess(Process process)
        {
            if (!ExternalMethods.AssignProcessToJobObject(m_handle.Value, process.Handle))
            {
                //GetLastErrorThrow();
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="T:System.Object"></see>.
        /// </returns>
        public override string ToString()
        {
            return m_name + "(" + m_handle + ")";
        }
    }

    /// <summary>
    /// Enumeration that directs a windows control action.
    /// </summary>
    [CLSCompliant(false)]
    public enum WindowsControl : uint
    {
        /// <summary>
        /// 
        /// </summary>
        Logoff = Win32Constants.EWX_LOGOFF,

        /// <summary>
        /// 
        /// </summary>
        ShutdownAndPowerOff = Win32Constants.EWX_POWEROFF,

        /// <summary>
        /// 
        /// </summary>
        ShutdownNoPowerOff = Win32Constants.EWX_SHUTDOWN,

        /// <summary>
        /// 
        /// </summary>
        Restart = Win32Constants.EWX_REBOOT,

        /// <summary>
        /// 
        /// </summary>
        RestartApps = Win32Constants.EWX_RESTARTAPPS,
    }

    /// <summary>
    /// 
    /// </summary>
    public static class Win32Enums
    {
        /// <summary>
        /// 
        /// </summary>
        public enum INSTALL_GAC_REFERENCE
        {
            /// <summary>
            /// The assembly is referenced by an application that is represented
            /// by a file in the file system.
            /// </summary>
            ApplicationInFilesystem,

            /// <summary>
            /// The assembly is referenced by an application that appears
            /// in Add/Remove Programs.
            /// </summary>
            ProgramInAddRemoveProgramsList,

            /// <summary>
            /// The assembly is referenced by an application that is only
            /// represented by an opaque string. The GAC does not perform existence checking
            /// for opaque references when you remove this.
            /// </summary>
            OpaqueProgram,

            /// <summary>
            /// 
            /// </summary>
            MSI
        }

        /// <summary>
        /// 
        /// </summary>
        public enum AssemblyCacheUninstallDisposition
        {
            /// <summary>
            /// The assembly files have been removed from the GAC.
            /// </summary>
            Uninstalled = 1,

            /// <summary>
            /// An application is using the assembly. This value is returned on Microsoft Windows 95 and Microsoft Windows 98.
            /// </summary>
            StillInUs = 2,

            /// <summary>
            /// The assembly does not exist in the GAC.
            /// </summary>
            AlreadyUninstalled = 3,

            /// <summary>
            /// Not used.
            /// </summary>
            DeletePending = 4,

            /// <summary>
            /// The assembly has not been removed from the GAC because another application reference exists.
            /// </summary>
            HasInstallReferences = 5,

            /// <summary>
            /// The reference that is specified in pRefData is not found in the GAC.
            /// </summary>
            ReferenceNotFound = 6
        }

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum CREATE_ASM_NAME_OBJ_FLAGS
        {
            /// <summary>
            /// If this flag is specified, the szAssemblyName parameter is a full assembly name and is parsed to the individual properties. If the flag is not specified, szAssemblyName is the "Name" portion of the assembly name.
            /// </summary>
            CANOF_PARSE_DISPLAY_NAME = 0x1,

            /// <summary>
            /// If this flag is specified, certain properties, such as processor architecture, are set to their default values.
            /// </summary>
            CANOF_SET_DEFAULT_VALUES = 0x2
        }

        /// <summary>
        /// The ASM_NAME enumeration property ID describes the valid names of the name-value pairs in an assembly name.
        /// </summary>
        public enum ASM_NAME
        {
            /// <summary>
            /// Property ID for the assembly's public key. The value is a byte array.
            /// </summary>
            ASM_NAME_PUBLIC_KEY = 0,

            /// <summary>
            /// Property ID for the assembly's public key token. The value is a byte array.
            /// </summary>
            ASM_NAME_PUBLIC_KEY_TOKEN,

            /// <summary>
            /// Property ID for a reserved name-value pair. The value is a byte array.
            /// </summary>
            ASM_NAME_HASH_VALUE,

            /// <summary>
            /// Property ID for the assembly's simple name. The value is a string value.
            /// </summary>
            ASM_NAME_NAME,

            /// <summary>
            /// Property ID for the assembly's major version. The value is a WORD value.
            /// </summary>
            ASM_NAME_MAJOR_VERSION,

            /// <summary>
            /// Property ID for the assembly's minor version. The value is a WORD value.
            /// </summary>
            ASM_NAME_MINOR_VERSION,

            /// <summary>
            /// Property ID for the assembly's build version. The value is a WORD value.
            /// </summary>
            ASM_NAME_BUILD_NUMBER,

            /// <summary>
            /// Property ID for the assembly's revision version. The value is a WORD value.
            /// </summary>
            ASM_NAME_REVISION_NUMBER,

            /// <summary>
            /// Property ID for the assembly's culture. The value is a string value.
            /// </summary>
            ASM_NAME_CULTURE,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_PROCESSOR_ID_ARRAY,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_OSINFO_ARRAY,

            /// <summary>
            /// Property ID for a reserved name-value pair. The value is a DWORD value.
            /// </summary>
            ASM_NAME_HASH_ALGID,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_ALIAS,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_CODEBASE_URL,

            /// <summary>
            /// Property ID for a reserved name-value pair. The value is a FILETIME structure.
            /// </summary>
            ASM_NAME_CODEBASE_LASTMOD,

            /// <summary>
            /// Property ID for the assembly as a simply named assembly that does not have a public key.
            /// </summary>
            ASM_NAME_NULL_PUBLIC_KEY,

            /// <summary>
            /// Property ID for the assembly as a simply named assembly that does not have a public key token.
            /// </summary>
            ASM_NAME_NULL_PUBLIC_KEY_TOKEN,

            /// <summary>
            /// Property ID for a reserved name-value pair. The value is a string value.
            /// </summary>
            ASM_NAME_CUSTOM,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_NULL_CUSTOM,

            /// <summary>
            /// Property ID for a reserved name-value pair.
            /// </summary>
            ASM_NAME_MVID,

            /// <summary>
            /// Reserved.
            /// </summary>
            ASM_NAME_MAX_PARAMS
        }

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum ASM_DISPLAY_FLAGS
        {
            /// <summary>
            /// Includes the version number as part of the display name.
            /// </summary>
            ASM_DISPLAYF_VERSION = 0x1,

            /// <summary>
            /// Includes the culture.
            /// </summary>
            ASM_DISPLAYF_CULTURE = 0x2,

            /// <summary>
            /// Includes the public key token.
            /// </summary>
            ASM_DISPLAYF_PUBLIC_KEY_TOKEN = 0x4,

            /// <summary>
            /// Includes the public key.
            /// </summary>
            ASM_DISPLAYF_PUBLIC_KEY = 0x8,

            /// <summary>
            /// Includes the custom part of the assembly name.
            /// </summary>
            ASM_DISPLAYF_CUSTOM = 0x10,

            /// <summary>
            /// Includes the processor architecture.
            /// </summary>
            ASM_DISPLAYF_PROCESSORARCHITECTURE = 0x20,

            /// <summary>
            /// Includes the language ID.
            /// </summary>
            ASM_DISPLAYF_LANGUAGEID = 0x40
        }

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum ASM_CMP_FLAGS
        {
            /// <summary>
            /// Compare the name portion of the assembly names.
            /// </summary>
            ASM_CMPF_NAME = 0x1,

            /// <summary>
            /// Compare the major version portion of the assembly names.
            /// </summary>
            ASM_CMPF_MAJOR_VERSION = 0x2,

            /// <summary>
            /// Compare the minor version portion of the assembly names.
            /// </summary>
            ASM_CMPF_MINOR_VERSION = 0x4,

            /// <summary>
            /// Compare the build version portion of the assembly names.
            /// </summary>
            ASM_CMPF_BUILD_NUMBER = 0x8,

            /// <summary>
            /// Compare the revision version portion of the assembly names.
            /// </summary>
            ASM_CMPF_REVISION_NUMBER = 0x10,

            /// <summary>
            /// Compare the public key token portion of the assembly names.
            /// </summary>
            ASM_CMPF_PUBLIC_KEY_TOKEN = 0x20,

            /// <summary>
            /// Compare the culture portion of the assembly names.
            /// </summary>
            ASM_CMPF_CULTURE = 0x40,

            /// <summary>
            /// Compare the custom portion of the assembly names.
            /// </summary>
            ASM_CMPF_CUSTOM = 0x80,

            /// <summary>
            /// Compare all portions of the assembly names.
            /// </summary>
            ASM_CMPF_ALL = ASM_CMPF_NAME | ASM_CMPF_MAJOR_VERSION | ASM_CMPF_MINOR_VERSION |
                           ASM_CMPF_REVISION_NUMBER | ASM_CMPF_BUILD_NUMBER |
                           ASM_CMPF_PUBLIC_KEY_TOKEN | ASM_CMPF_CULTURE | ASM_CMPF_CUSTOM,

            /// <summary>
            /// Ignore the version number to compare assemblies with simple names.
            /// 
            /// For strongly named assemblies, ASM_CMPF_DEFAULT==ASM_CMPF_ALL.
            /// For simply named assemblies, this is also true. However, when
            /// performing IAssemblyName::IsEqual, the build number/revision 
            /// number will be removed from the comparison.
            /// </summary>
            ASM_CMPF_DEFAULT = 0x100
        }

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum ASM_CACHE_FLAGS
        {
            /// <summary>
            /// Enumerates the cache of precompiled assemblies by using Ngen.exe.
            /// </summary>
            ASM_CACHE_ZAP = 0x1,

            /// <summary>
            /// Enumerates the GAC.
            /// </summary>
            ASM_CACHE_GAC = 0x2,

            /// <summary>
            /// Enumerates the assemblies that have been downloaded on-demand or that have been shadow-copied.
            /// </summary>
            ASM_CACHE_DOWNLOAD = 0x4
        }

        /// <summary>
        /// 
        /// </summary>
        public enum IASSEMBLYCACHE_INSTALL_FLAG
        {
            /// <summary>
            /// If the assembly is already installed in the GAC and the file version numbers of the assembly being installed are the same or later, the files are replaced.
            /// </summary>
            IASSEMBLYCACHE_INSTALL_FLAG_REFRESH = 1,

            /// <summary>
            /// The files of an existing assembly are overwritten regardless of their version number.
            /// </summary>
            IASSEMBLYCACHE_INSTALL_FLAG_FORCE_REFRESH = 2
        }

        /// <summary>
        /// 
        /// </summary>
        public enum JobObjectInfoClass
        {
            /// <summary>
            /// 
            /// </summary>
            JobObjectBasicAccountingInformation = 1,

            /// <summary>
            /// 
            /// </summary>
            JobObjectBasicLimitInformation = 2,

            /// <summary>
            /// 
            /// </summary>
            JobObjectBasicProcessIdList = 3,

            /// <summary>
            /// 
            /// </summary>
            JobObjectBasicUIRestrictions = 4,

            /// <summary>
            /// 
            /// </summary>
            JobObjectSecurityLimitInformation = 5,

            /// <summary>
            /// 
            /// </summary>
            JobObjectEndOfJobTimeInformation = 6,

            /// <summary>
            /// 
            /// </summary>
            JobObjectAssociateCompletionPortInformation = 7,

            /// <summary>
            /// 
            /// </summary>
            JobObjectBasicAndIoAccountingInformation = 8,

            /// <summary>
            /// 
            /// </summary>
            JobObjectExtendedLimitInformation = 9
        }

        /// <summary>
        /// 
        /// </summary>
        [Flags]
        public enum LimitFlags
        {
            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_ACTIVE_PROCESS = 0x00000008,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_AFFINITY = 0x00000010,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_BREAKAWAY_OK = 0x00000800,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_DIE_ON_UNHANDLED_EXCEPTION = 0x00000400,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_JOB_MEMORY = 0x00000200,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_JOB_TIME = 0x00000004,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE = 0x00002000,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_PRESERVE_JOB_TIME = 0x00000040,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_PRIORITY_CLASS = 0x00000020,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_PROCESS_MEMORY = 0x00000100,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_PROCESS_TIME = 0x00000002,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_SCHEDULING_CLASS = 0x00000080,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_SILENT_BREAKAWAY_OK = 0x00001000,

            /// <summary>
            /// 
            /// </summary>
            JOB_OBJECT_LIMIT_WORKINGSET = 0x00000001
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class Win32Structures
    {
        /// <summary>
        /// The FUSION_INSTALL_REFERENCE structure represents a reference
        /// that is made when an application has installed an assembly in the GAC.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        [CLSCompliant(false)]
        public struct FUSION_INSTALL_REFERENCE
        {
            /// <summary>
            /// The size of the structure in bytes.
            /// </summary>
            public uint cbSize;

            /// <summary>
            /// Reserved, must be zero.
            /// </summary>
            public uint dwFlags;

            /// <summary>
            /// The entity that adds the reference.
            /// 
            /// Possible values for the guidScheme field can be one of the following:
            /// FUSION_REFCOUNT_MSI_GUID - The assembly is referenced by an application that has been installed by using Windows Installer. The szIdentifier field is set to MSI, and szNonCannonicalData is set to Windows Installer. This scheme must only be used by Windows Installer itself.
            /// FUSION_REFCOUNT_UNINSTALL_SUBKEY_GUID - The assembly is referenced by an application that appears in Add/Remove Programs. The szIdentifier field is the token that is used to register the application with Add/Remove programs.
            /// FUSION_REFCOUNT_FILEPATH_GUID - The assembly is referenced by an application that is represented by a file in the file system. The szIdentifier field is the path to this file.
            /// FUSION_REFCOUNT_OPAQUE_STRING_GUID - The assembly is referenced by an application that is only represented by an opaque string. The szIdentifier is this opaque string. The GAC does not perform existence checking for opaque references when you remove this.
            /// </summary>
            public Guid guidScheme;

            /// <summary>
            /// A unique string that identifies the application that installed the assembly.
            /// </summary>
            public string szIdentifier;

            /// <summary>
            /// A string that is only understood by the entity that adds the reference. The GAC only stores this string.
            /// </summary>
            public string szNonCannonicalData;
        }

        /// <summary>
        /// The ASSEMBLY_INFO structure represents information about an
        /// assembly in the assembly cache.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        [CLSCompliant(false)]
        public struct ASSEMBLY_INFO
        {
            /// <summary>
            /// Size of the structure in bytes. Permits additions
            /// to the structure in future version of the .NET Framework.
            /// </summary>
            public uint cbAssemblyInfo;

            /// <summary>
            /// Indicates one or more of the ASSEMBLYINFO_FLAG_* bits.
            /// 
            /// dwAssemblyFlags can have one of the following values:
            /// ASSEMBLYINFO_FLAG__INSTALLED - Indicates that the assembly is actually installed. Always set in current version of the .NET Framework.
            /// ASSEMBLYINFO_FLAG__PAYLOADRESIDENT - Never set in the current version of the .NET Framework.
            /// </summary>
            public uint dwAssemblyFlags;

            /// <summary>
            /// The size of the files that make up the assembly in kilobytes (KB).
            /// </summary>
            public ulong uliAssemblySizeInKB;

            /// <summary>
            /// A pointer to a string buffer that holds the current path of the directory that contains the files that make up the assembly. The path must end with a zero.
            /// </summary>
            public string pszCurrentAssemblyPathBuf;

            /// <summary>
            /// Size of the buffer that the pszCurrentAssemblyPathBug field points to.
            /// </summary>
            public uint cchBuf;
        }

        /// <summary>
        /// The SECURITY_ATTRIBUTES structure contains the security descriptor for an object and specifies whether the handle retrieved by specifying this structure is inheritable. This structure provides security settings for objects created by various functions, such as CreateFile, CreatePipe, CreateProcess, RegCreateKeyEx, or RegSaveKeyEx.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct SECURITY_ATTRIBUTES
        {
            /// <summary>
            /// The size, in bytes, of this structure. Set this value to the size of the SECURITY_ATTRIBUTES structure.
            /// </summary>
            public int nLength;

            /// <summary>
            /// A pointer to a security descriptor for the object that controls the sharing of it. If NULL is specified for this member, the object is assigned the default security descriptor of the calling process. This is not the same as granting access to everyone by assigning a NULL discretionary access control list (DACL). The default security descriptor is based on the default DACL of the access token belonging to the calling process. By default, the default DACL in the access token of a process allows access only to the user represented by the access token. If other users must access the object, you can either create a security descriptor with the appropriate access, or add ACEs to the DACL that grants access to a group of users.
            /// </summary>
            public IntPtr lpSecurityDescriptor;

            /// <summary>
            /// A Boolean value that specifies whether the returned handle is inherited when a new process is created. If this member is TRUE, the new process inherits the handle.
            /// </summary>
            public int bInheritHandle;
        }

        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        [CLSCompliant(false)]
        public struct JOBOBJECT_BASIC_LIMIT_INFORMATION
        {
            /// <summary>
            /// 
            /// </summary>
            public long PerProcessUserTimeLimit;

            /// <summary>
            /// 
            /// </summary>
            public long PerJobUserTimeLimit;

            /// <summary>
            /// 
            /// </summary>
            public Win32Enums.LimitFlags LimitFlags;

            /// <summary>
            /// 
            /// </summary>
            public uint MinimumWorkingSetSize;

            /// <summary>
            /// 
            /// </summary>
            public uint MaximumWorkingSetSize;

            /// <summary>
            /// 
            /// </summary>
            public int ActiveProcessLimit;

            /// <summary>
            /// 
            /// </summary>
            public IntPtr Affinity;

            /// <summary>
            /// 
            /// </summary>
            public int PriorityClass;

            /// <summary>
            /// 
            /// </summary>
            public int SchedulingClass;
        }

        /// <summary>
        /// 
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct SYSTEMTIME
        {
            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wYear;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wMonth;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wDayOfWeek;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wDay;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wHour;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wMinute;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wSecond;

            /// <summary>
            /// 
            /// </summary>
            [MarshalAs(UnmanagedType.U2)]
            public short wMilliseconds;
        }

        /// <summary>
        /// The TimeZoneInformation structure specifies information specific to the time zone.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        public struct TIME_ZONE_INFORMATION
        {
            /// <summary>
            /// Current bias for local time translation on this computer, in minutes. The bias is the difference, in minutes, between Coordinated Universal Time (UTC) and local time. All translations between UTC and local time are based on the following formula:
            /// <para>UTC = local time + bias</para>
            /// <para>This member is required.</para>
            /// </summary>
            public int bias;

            /// <summary>
            /// Pointer to a null-terminated string associated with standard time. For example, "EST" could indicate Eastern Standard Time. The string will be returned unchanged by the GetTimeZoneInformation function. This string can be empty.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string standardName;

            /// <summary>
            /// A SystemTime structure that contains a date and local time when the transition from daylight saving time to standard time occurs on this operating system. If the time zone does not support daylight saving time or if the caller needs to disable daylight saving time, the wMonth member in the SystemTime structure must be zero. If this date is specified, the DaylightDate value in the TimeZoneInformation structure must also be specified. Otherwise, the system assumes the time zone data is invalid and no changes will be applied.
            /// <para>To select the correct day in the month, set the wYear member to zero, the wHour and wMinute members to the transition time, the wDayOfWeek member to the appropriate weekday, and the wDay member to indicate the occurence of the day of the week within the month (first through fifth).</para>
            /// <para>Using this notation, specify the 2:00a.m. on the first Sunday in April as follows: wHour = 2, wMonth = 4, wDayOfWeek = 0, wDay = 1. Specify 2:00a.m. on the last Thursday in October as follows: wHour = 2, wMonth = 10, wDayOfWeek = 4, wDay = 5.</para>
            /// </summary>
            public SYSTEMTIME standardDate;

            /// <summary>
            /// Bias value to be used during local time translations that occur during standard time. This member is ignored if a value for the StandardDate member is not supplied.
            /// <para>This value is added to the value of the Bias member to form the bias used during standard time. In most time zones, the value of this member is zero.</para>
            /// </summary>
            public int standardBias;

            /// <summary>
            /// Pointer to a null-terminated string associated with daylight saving time. For example, "PDT" could indicate Pacific Daylight Time. The string will be returned unchanged by the GetTimeZoneInformation function. This string can be empty.
            /// </summary>
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string daylightName;

            /// <summary>
            /// A SystemTime structure that contains a date and local time when the transition from standard time to daylight saving time occurs on this operating system. If the time zone does not support daylight saving time or if the caller needs to disable daylight saving time, the wMonth member in the SystemTime structure must be zero. If this date is specified, the StandardDate value in the TimeZoneInformation structure must also be specified. Otherwise, the system assumes the time zone data is invalid and no changes will be applied.
            /// <para>To select the correct day in the month, set the wYear member to zero, the wHour and wMinute members to the transition time, the wDayOfWeek member to the appropriate weekday, and the wDay member to indicate the occurence of the day of the week within the month (first through fifth).</para>
            /// </summary>
            public SYSTEMTIME daylightDate;

            /// <summary>
            /// Bias value to be used during local time translations that occur during daylight saving time. This member is ignored if a value for the DaylightDate member is not supplied.
            /// <para>This value is added to the value of the Bias member to form the bias used during daylight saving time. In most time zones, the value of this member is �60.</para>
            /// </summary>
            public int daylightBias;
        }
    }

    /// <summary>
    /// Win32 constants
    /// </summary>
    [CLSCompliant(false)]
    public static class Win32Constants
    {
        /// <summary>
        /// Success
        /// </summary>
        public const int S_OK = 0x00000000;

        /// <summary>
        /// 
        /// </summary>
        public const int S_FALSE = 0x00000001;

        /// <summary>
        /// Shuts down all processes running in the logon session of the process that called the ExitWindowsEx function. Then it logs the user off.
        /// This flag can be used only by processes running in an interactive user's logon session.
        /// </summary>
        public const uint EWX_LOGOFF = 0;

        /// <summary>
        /// Shuts down the system and turns off the power. The system must support the power-off feature.
        /// The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section.
        /// </summary>
        public const uint EWX_POWEROFF = 0x00000008;

        /// <summary>
        /// Shuts down the system and then restarts the system.
        /// The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section.
        /// </summary>
        public const uint EWX_REBOOT = 0x00000002;

        /// <summary>
        /// Shuts down the system and then restarts it, as well as any applications that have been registered for restart using the RegisterApplicationRestart function. These application receive the WM_QUERYENDSESSION message with lParam set to the ENDSESSION_CLOSEAPP value. For more information, see Guidelines for Applications.
        /// </summary>
        public const uint EWX_RESTARTAPPS = 0x00000040;

        /// <summary>
        /// Shuts down the system to a point at which it is safe to turn off the power. All file buffers have been flushed to disk, and all running processes have stopped.
        /// The calling process must have the SE_SHUTDOWN_NAME privilege. For more information, see the following Remarks section.
        /// Specifying this flag will not turn off the power even if the system supports the power-off feature. You must specify EWX_POWEROFF to do this.
        /// Windows XP SP1:  If the system supports the power-off feature, specifying this flag turns off the power.
        /// </summary>
        public const uint EWX_SHUTDOWN = 0x00000001;

        /// <summary>
        /// This flag has no effect if terminal services is enabled. Otherwise, the system does not send the WM_QUERYENDSESSION and WM_ENDSESSION messages. This can cause applications to lose data. Therefore, you should only use this flag in an emergency.
        /// </summary>
        public const uint EWX_FORCE = 0x00000004;

        /// <summary>
        /// Forces processes to terminate if they do not respond to the WM_QUERYENDSESSION or WM_ENDSESSION message within the timeout interval. For more information, see the Remarks.
        /// Windows NT and Windows Me/98/95:  This value is not supported.
        /// </summary>
        public const uint EWX_FORCEIFHUNG = 0x00000010;

        /// <summary>
        /// Application issue.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_APPLICATION = 0x00040000;

        /// <summary>
        /// Hardware issue.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_HARDWARE = 0x00010000;

        /// <summary>
        /// The InitiateSystemShutdown function was used instead of InitiateSystemShutdownEx.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_LEGACY_API = 0x00070000;

        /// <summary>
        /// Operating system issue.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_OPERATINGSYSTEM = 0x00020000;

        /// <summary>
        /// Other issue.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_OTHER = 0x00000000;

        /// <summary>
        /// Power failure.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_POWER = 0x00060000;

        /// <summary>
        /// Software issue.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_SOFTWARE = 0x00030000;

        /// <summary>
        /// System failure.
        /// </summary>
        public const uint SHTDN_REASON_MAJOR_SYSTEM = 0x00050000;

        /// <summary>
        /// Blue screen crash event.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_BLUESCREEN = 0x0000000F;

        /// <summary>
        /// Unplugged.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_CORDUNPLUGGED = 0x0000000b;

        /// <summary>
        /// Disk.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_DISK = 0x00000007;

        /// <summary>
        /// Environment.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_ENVIRONMENT = 0x0000000c;

        /// <summary>
        /// Driver.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_HARDWARE_DRIVER = 0x0000000d;

        /// <summary>
        /// Hot fix.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_HOTFIX = 0x00000011;

        /// <summary>
        /// Hot fix uninstallation.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_HOTFIX_UNINSTALL = 0x00000017;

        /// <summary>
        /// Unresponsive.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_HUNG = 0x00000005;

        /// <summary>
        /// Installation.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_INSTALLATION = 0x00000002;

        /// <summary>
        /// Maintenance.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_MAINTENANCE = 0x00000001;

        /// <summary>
        /// MMC issue.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_MMC = 0x00000019;

        /// <summary>
        /// Network connectivity.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_NETWORK_CONNECTIVITY = 0x00000014;

        /// <summary>
        /// Network card.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_NETWORKCARD = 0x00000009;

        /// <summary>
        /// Other issue.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_OTHER = 0x00000000;

        /// <summary>
        /// Other driver event.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_OTHERDRIVER = 0x0000000e;

        /// <summary>
        /// Power supply.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_POWER_SUPPLY = 0x0000000a;

        /// <summary>
        /// Processor.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_PROCESSOR = 0x00000008;

        /// <summary>
        /// Reconfigure.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_RECONFIG = 0x00000004;

        /// <summary>
        /// Security issue.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_SECURITY = 0x00000013;

        /// <summary>
        /// Security patch.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_SECURITYFIX = 0x00000012;

        /// <summary>
        /// Security patch uninstallation.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_SECURITYFIX_UNINSTALL = 0x00000018;

        /// <summary>
        /// Service pack.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_SERVICEPACK = 0x00000010;

        /// <summary>
        /// Service pack uninstallation.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_SERVICEPACK_UNINSTALL = 0x00000016;

        /// <summary>
        /// Terminal Services.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_TERMSRV = 0x00000020;

        /// <summary>
        /// Unstable.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_UNSTABLE = 0x00000006;

        /// <summary>
        /// Upgrade.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_UPGRADE = 0x00000003;

        /// <summary>
        /// WMI issue.
        /// </summary>
        public const uint SHTDN_REASON_MINOR_WMI = 0x00000015;

        /// <summary>
        /// The shutdown was planned. The system generates a System State Data (SSD) file. This file contains system state information such as the processes, threads, memory usage, and configuration.
        /// If this flag is not present, the shutdown was unplanned. Notification and reporting options are controlled by a set of policies. For example, after logging in, the system displays a dialog box reporting the unplanned shutdown if the policy has been enabled. An SSD file is created only if the SSD policy is enabled on the system. The administrator can use Windows Error Reporting to send the SSD data to a central location, or to Microsoft.
        /// </summary>
        public const uint SHTDN_REASON_FLAG_PLANNED = 0x80000000;
    }

    /// <summary>
    /// Class that contains PInvoke methods into Win32
    /// </summary>
    [CLSCompliant(false)]
    public static class ExternalMethods
    {
        /// <summary>
        /// Logs off the interactive user, shuts down the system, or shuts down and restarts the system. It sends the WM_QUERYENDSESSION message to all applications to determine if they can be terminated.
        /// http://msdn2.microsoft.com/en-us/library/aa376868.aspx
        /// </summary>
        /// <param name="uFlags"></param>
        /// <param name="dwReason">The reason for initiating the shutdown. This parameter must be one of the system shutdown reason codes.
        /// If this parameter is zero, the SHTDN_REASON_FLAG_PLANNED reason code will not be set and therefore the default action is an undefined shutdown that is logged as "No title for this reason could be found". By default, it is also an unplanned shutdown. Depending on how the system is configured, an unplanned shutdown triggers the creation of a file that contains the system state information, which can delay shutdown. Therefore, do not use zero for this parameter.</param>
        /// <returns>If the function succeeds, the return value is nonzero. Because the function executes asynchronously, a nonzero return value indicates that the shutdown has been initiated. It does not indicate whether the shutdown will succeed. It is possible that the system, the user, or another application will abort the shutdown.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.</returns>
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool ExitWindowsEx(uint uFlags, uint dwReason);

        /// <summary>
        /// To obtain an instance of the CreateAssemblyCache API
        /// </summary>
        /// <param name="ppAsmCache">Pointer to return IAssemblyCache</param>
        /// <param name="dwReserved">Reserved, must be zero.</param>
        [DllImport("fusion.dll", SetLastError = true, PreserveSig = false)]
        public static extern void CreateAssemblyCache(out Win32Interfaces.IAssemblyCache ppAsmCache, uint dwReserved);

        /// <summary>
        /// An instance of IAssemblyName is obtained by calling the CreateAssemblyNameObject API
        /// </summary>
        /// <param name="ppAssemblyNameObj">Pointer to a memory location that receives the IAssemblyName pointer that is created.</param>
        /// <param name="szAssemblyName">A string representation of the assembly name or of a full assembly reference that is determined by dwFlags. The string representation can be null.</param>
        /// <param name="dwFlags">Zero or more of the bits that are defined in the CREATE_ASM_NAME_OBJ_FLAGS enumeration.</param>
        /// <param name="pvReserved">Must be null.</param>
        [DllImport("fusion.dll", SetLastError = true, CharSet = CharSet.Auto, PreserveSig = false)]
        public static extern void CreateAssemblyNameObject(
            out Win32Interfaces.IAssemblyName ppAssemblyNameObj,
            string szAssemblyName,
            Win32Enums.CREATE_ASM_NAME_OBJ_FLAGS dwFlags,
            IntPtr pvReserved
        );

        /// <summary>
        /// To obtain an instance of the CreateAssemblyEnum API, call the CreateAssemblyNameObject API
        /// </summary>
        /// <param name="pEnum">Pointer to a memory location that contains the IAssemblyEnum pointer.</param>
        /// <param name="pUnkReserved">Must be null.</param>
        /// <param name="pName">An assembly name that is used to filter the enumeration. Can be null to enumerate all assemblies in the GAC.</param>
        /// <param name="dwFlags">Exactly one bit from the ASM_CACHE_FLAGS enumeration.</param>
        /// <param name="pvReserved">Must be NULL.</param>
        [DllImport("fusion.dll", SetLastError = true, PreserveSig = false)]
        public static extern void CreateAssemblyEnum(
            out Win32Interfaces.IAssemblyEnum pEnum,
            IntPtr pUnkReserved,
            Win32Interfaces.IAssemblyName pName,
            Win32Enums.ASM_CACHE_FLAGS dwFlags,
            IntPtr pvReserved
        );

        /// <summary>
        /// To obtain an instance of the CreateInstallReferenceEnum API, call the CreateInstallReferenceEnum API
        /// </summary>
        /// <param name="ppRefEnum">A pointer to a memory location that receives the IInstallReferenceEnum pointer.</param>
        /// <param name="pName">The assembly name for which the references are enumerated.</param>
        /// <param name="dwFlags">Must be zero.</param>
        /// <param name="pvReserved">Must be null.</param>
        [DllImport("fusion.dll", SetLastError = true, PreserveSig = false)]
        public static extern void CreateInstallReferenceEnum(
            out Win32Interfaces.IInstallReferenceEnum ppRefEnum,
            Win32Interfaces.IAssemblyName pName,
            uint dwFlags,
            IntPtr pvReserved
        );

        /// <summary>
        /// The GetCachePath API returns the storage location of the GAC.
        /// </summary>
        /// <param name="dwCacheFlags">Exactly one of the bits defined in the ASM_CACHE_FLAGS enumeration.</param>
        /// <param name="pwzCachePath">Pointer to a buffer that is to receive the path of the GAC as a Unicode string.</param>
        /// <param name="pcchPath">Length of the pwszCachePath buffer, in Unicode characters.</param>
        [DllImport("fusion.dll", SetLastError = true, CharSet = CharSet.Auto, PreserveSig = false)]
        public static extern void GetCachePath(
            Win32Enums.ASM_CACHE_FLAGS dwCacheFlags,
            [MarshalAs(UnmanagedType.LPWStr)] StringBuilder pwzCachePath,
            ref uint pcchPath
        );

        /// <summary>
        /// The GetDiskFreeSpaceEx function retrieves information about the amount of space that is available on a disk volume, which is the total amount of space, the total amount of free space, and the total amount of free space available to the user that is associated with the calling thread.
        /// </summary>
        /// <param name="lpDirectoryName">A pointer to a null-terminated string that specifies a directory on a disk.
        /// If this parameter is NULL, the function uses the root of the current disk.
        /// If this parameter is a UNC name, it must include a trailing backslash, for example, \\MyServer\MyShare\.
        /// This parameter does not have to specify the root directory on a disk. The function accepts any directory on a disk.
        /// The calling application must have FILE_LIST_DIRECTORY access rights for this directory.</param>
        /// <param name="lpFreeBytesAvailableToCaller">A pointer to a variable that receives the total number of free bytes on a disk that are available to the user who is associated with the calling thread.
        /// This parameter can be NULL.
        /// Windows Me/98/95:  This parameter cannot be NULL.
        /// If per-user quotas are being used, this value may be less than the total number of free bytes on a disk.</param>
        /// <param name="lpTotalNumberOfBytes">A pointer to a variable that receives the total number of bytes on a disk that are available to the user who is associated with the calling thread.
        /// This parameter can be NULL.
        /// Windows Me/98/95 and Windows NT 4.0:  This parameter cannot be NULL.
        /// If per-user quotas are being used, this value may be less than the total number of bytes on a disk.
        /// To determine the total number of bytes on a disk or volume, use IOCTL_DISK_GET_LENGTH_INFO.</param>
        /// <param name="lpTotalNumberOfFreeBytes">A pointer to a variable that receives the total number of free bytes on a disk.
        /// This parameter can be NULL.</param>
        /// <returns>If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is 0 (zero). To get extended error information, call GetLastError.</returns>
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern bool GetDiskFreeSpaceEx(
            string lpDirectoryName,
            out long lpFreeBytesAvailableToCaller,
            out long lpTotalNumberOfBytes,
            out long lpTotalNumberOfFreeBytes
        );

        /// <summary>
        /// Creates or opens a job object.
        /// </summary>
        /// <param name="lpJobAttributes">A pointer to a SECURITY_ATTRIBUTES structure that specifies the security descriptor for the job object and determines whether child processes can inherit the returned handle. If lpJobAttributes is NULL, the job object gets a default security descriptor and the handle cannot be inherited. The ACLs in the default security descriptor for a job object come from the primary or impersonation token of the creator.</param>
        /// <param name="lpName">The name of the job. The name is limited to MAX_PATH characters. Name comparison is case-sensitive.
        /// If lpName is NULL, the job is created without a name.
        /// If lpName matches the name of an existing event, semaphore, mutex, waitable timer, or file-mapping object, the function fails and the GetLastError function returns ERROR_INVALID_HANDLE. This occurs because these objects share the same name space.
        /// The object can be created in a private namespace. For more information, see Object Namespaces.
        /// Terminal Services:  The name can have a "Global\" or "Local\" prefix to explicitly create the object in the global or session name space. The remainder of the name can contain any character except the backslash character (\). For more information, see Kernel Object Namespaces.
        /// Windows 2000:  If Terminal Services is not running, the "Global\" and "Local\" prefixes are ignored. The remainder of the name can contain any character except the backslash character.</param>
        /// <returns>If the function succeeds, the return value is a handle to the job object. The handle has the JOB_OBJECT_ALL_ACCESS access right. If the object existed before the function call, the function returns a handle to the existing job object and GetLastError returns ERROR_ALREADY_EXISTS.
        /// If the function fails, the return value is NULL. To get extended error information, call GetLastError.</returns>
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern IntPtr CreateJobObject(
            [In] ref Win32Structures.SECURITY_ATTRIBUTES lpJobAttributes,
            string lpName
        );

        /// <summary>
        /// Closes an open object handle.
        /// </summary>
        /// <param name="hObject">A valid handle to an open object.</param>
        /// <returns>If the function succeeds, the return value is nonzero.
        /// If the function fails, the return value is zero. To get extended error information, call GetLastError.
        /// If the application is running under a debugger, the function will throw an exception if it receives either a handle value that is not valid or a pseudo-handle value. This can happen if you close a handle twice, or if you call CloseHandle on a handle returned by the FindFirstFile function.</returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CloseHandle(IntPtr hObject);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hJob"></param>
        /// <param name="JobObjectInfoClass"></param>
        /// <param name="lpJobObjectInfo"></param>
        /// <param name="cbJobObjectInfoLength"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "SetInformationJobObject")]
        internal static extern bool SetInformationJobObjectLimit(
            IntPtr hJob,
            Win32Enums.JobObjectInfoClass JobObjectInfoClass,
            ref Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION lpJobObjectInfo,
            int cbJobObjectInfoLength
        );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hJob"></param>
        /// <param name="JobObjectInfoClass"></param>
        /// <param name="lpJobObjectInfo"></param>
        /// <param name="cbJobObjectInfoLength"></param>
        /// <param name="lpReturnLength"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true, EntryPoint = "QueryInformationJobObject")]
        internal static extern bool QueryInformationJobObjectLimit(
            IntPtr hJob,
            Win32Enums.JobObjectInfoClass JobObjectInfoClass,
            out Win32Structures.JOBOBJECT_BASIC_LIMIT_INFORMATION lpJobObjectInfo,
            int cbJobObjectInfoLength,
            out int lpReturnLength
        );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hJob"></param>
        /// <param name="hProcess"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool AssignProcessToJobObject(
            IntPtr hJob,
            IntPtr hProcess
        );

        /// <summary>
        /// Gets the system time.
        /// </summary>
        /// <param name="st">The st.</param>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool GetSystemTime(
            out Win32Structures.SYSTEMTIME st
        );

        /// <summary>
        /// Gets the local time.
        /// </summary>
        /// <param name="st">The st.</param>
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool GetLocalTime(
            out Win32Structures.SYSTEMTIME st
        );

        /// <summary>
        /// Sets the time zone information.
        /// </summary>
        /// <param name="lpTimeZoneInformation">The lp time zone information.</param>
        /// <returns></returns>
        [DllImport("kernel32.dll")]
        public static extern bool SetTimeZoneInformation(
            [In] ref Win32Structures.TIME_ZONE_INFORMATION lpTimeZoneInformation
        );
    }
    
        /// <summary>
        /// 
        /// </summary>
        [CLSCompliant(false)]
        public static class Win32Interfaces
        {
            /// <summary>
            /// 
            /// </summary>
            [ComImport, Guid("E707DCDE-D1CD-11D2-BAB9-00C04F8ECEAE"),
                InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
            public interface IAssemblyCache
            {
                /// <summary>
                /// The IAssemblyCache::UninstallAssembly method removes a reference to an assembly from the GAC. If other applications hold no other references to the assembly, the files that make up the assembly are removed from the GAC.
                /// </summary>
                /// <param name="dwFlags">No flags defined. Must be zero.</param>
                /// <param name="pszAssemblyName">The name of the assembly. A zero-ended Unicode string.</param>
                /// <param name="pRefData">A pointer to a FUSION_INSTALL_REFERENCE structure. Although this is not recommended, this parameter can be null. The assembly is installed without an application reference, or all existing application references are gone.</param>
                /// <param name="pulDisposition">Pointer to an integer that indicates the action that is performed by the function.
                /// 
                /// If pulDisposition is not null, pulDisposition contains one of the following values:
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_UNINSTALLED - The assembly files have been removed from the GAC.
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_STILL_IN_USE - An application is using the assembly. This value is returned on Microsoft Windows 95 and Microsoft Windows 98.
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_ALREADY_UNINSTALLED - The assembly does not exist in the GAC.
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_DELETE_PENDING - Not used.
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_HAS_INSTALL_REFERENCES - The assembly has not been removed from the GAC because another application reference exists.
                /// IASSEMBLYCACHE_UNINSTALL_DISPOSITION_REFERENCE_NOT_FOUND - The reference that is specified in pRefData is not found in the GAC.
                /// </param>
                /// <returns>S_OK - The assembly has been uninstalled.
                /// S_FALSE - The operation succeeded, but the assembly was not removed from the GAC. The reason is described in pulDisposition.</returns>
                [PreserveSig]
                int UninstallAssembly(
                    int dwFlags,
                    [MarshalAs(UnmanagedType.LPWStr)] string pszAssemblyName,
                    [MarshalAs(UnmanagedType.LPArray)] Win32Structures.FUSION_INSTALL_REFERENCE[] pRefData,
                    out uint pulDisposition
                );

                /// <summary>
                /// The IAssemblyCache::QueryAssemblyInfo method retrieves information about an assembly from the GAC.
                /// </summary>
                /// <param name="dwFlags">One of QUERYASMINFO_FLAG_VALIDATE or QUERYASMINFO_FLAG_GETSIZE:
                /// *_VALIDATE - Performs validation of the files in the GAC against the assembly manifest, including hash verification and strong name signature verification.
                /// *_GETSIZE - Returns the size of all files in the assembly (disk footprint). If this is not specified, the ASSEMBLY_INFO::uliAssemblySizeInKB field is not modified.</param>
                /// <param name="pszAssemblyName">Name of the assembly that is queried.</param>
                /// <param name="pAsmInfo">Pointer to the returned ASSEMBLY_INFO structure.</param>
                /// <returns></returns>
                [PreserveSig]
                void QueryAssemblyInfo(
                    uint dwFlags,
                    [MarshalAs(UnmanagedType.LPWStr)] string pszAssemblyName,
                    ref Win32Structures.ASSEMBLY_INFO pAsmInfo
                );

                /// <summary>
                /// The IAssemblyCache::InstallAssembly method adds a new assembly to the GAC. The assembly must be persisted in the file system and is copied to the GAC.
                /// </summary>
                /// <param name="dwFlags">At most, one of the bits of the IASSEMBLYCACHE_INSTALL_FLAG_* values can be specified:
                /// *_REFRESH - If the assembly is already installed in the GAC and the file version numbers of the assembly being installed are the same or later, the files are replaced.
                /// *_FORCE_REFRESH - The files of an existing assembly are overwritten regardless of their version number.</param>
                /// <param name="pszManifestFilePath">A string pointing to the dynamic-linked library (DLL) that contains the assembly manifest. Other assembly files must reside in the same directory as the DLL that contains the assembly manifest.</param>
                /// <param name="pRefData">A pointer to a FUSION_INSTALL_REFERENCE that indicates the application on whose behalf the assembly is being installed. Although this is not recommended, this parameter can be null, but this leaves the assembly without any application reference.</param>
                /// <returns></returns>
                [PreserveSig]
                void InstallAssembly(
                    Win32Enums.IASSEMBLYCACHE_INSTALL_FLAG dwFlags,
                    [MarshalAs(UnmanagedType.LPWStr)] string pszManifestFilePath,
                    [MarshalAs(UnmanagedType.LPArray)] Win32Structures.FUSION_INSTALL_REFERENCE[] pRefData
                );
            }

            /// <summary>
            /// The IAssemblyName interface represents an assembly name. An assembly name includes a predetermined set of name-value pairs. The assembly name is described in detail in the .NET Framework SDK.
            /// </summary>
            [ComImport, Guid("CD193BC0-B4BC-11D2-9833-00C04FC31D2E"),
                InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
            public interface IAssemblyName
            {
                /// <summary>
                /// The IAssemblyName::SetProperty method adds a name-value pair to the assembly name, or, if a name-value pair with the same name already exists, modifies or deletes the value of a name-value pair.
                /// </summary>
                /// <param name="PropertyId">The ID that represents the name part of the name-value pair that is to be added or to be modified. Valid property IDs are defined in the ASM_NAME enumeration.</param>
                /// <param name="pvProperty">A pointer to a buffer that contains the value of the property.</param>
                /// <param name="cbProperty">The length of the pvProperty buffer in bytes. If cbProperty is zero, the name-value pair is removed from the assembly name.</param>
                /// <returns></returns>
                [PreserveSig]
                int SetProperty(Win32Enums.ASM_NAME PropertyId, IntPtr pvProperty, uint cbProperty);

                /// <summary>
                /// The IAssemblyName::GetProperty method retrieves the value of a name-value pair in the assembly name that specifies the name.
                /// </summary>
                /// <param name="PropertyId">The ID that represents the name of the name-value pair whose value is to be retrieved. Specified property IDs are defined in the ASM_NAME enumeration.</param>
                /// <param name="pvProperty">A pointer to a buffer that is to contain the value of the property.</param>
                /// <param name="pcbProperty">The length of the pvProperty buffer, in bytes.</param>
                /// <returns></returns>
                [PreserveSig]
                int GetProperty(Win32Enums.ASM_NAME PropertyId, IntPtr pvProperty, ref uint pcbProperty);

                /// <summary>
                /// The IAssemblyName::Finalize method freezes an assembly name. Additional calls to IAssemblyName::SetProperty are unsuccessful after this method has been called.
                /// </summary>
                /// <returns></returns>
                [PreserveSig]
                int Finalize();

                /// <summary>
                /// The IAssemblyName::GetDisplayName method returns a string representation of the assembly name.
                /// </summary>
                /// <param name="szDisplayName">A pointer to a buffer that is to contain the display name. The display name is returned in Unicode.</param>
                /// <param name="pccDisplayName">The size of the buffer in characters (on input). The length of the returned display name (on return).</param>
                /// <param name="dwDisplayFlags">One or more of the bits defined in the ASM_DISPLAY_FLAGS enumeration</param>
                /// <returns></returns>
                [PreserveSig]
                int GetDisplayName(
                    [Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder szDisplayName,
                    ref uint pccDisplayName,
                    Win32Enums.ASM_DISPLAY_FLAGS dwDisplayFlags
                );

                /// <summary>
                /// The IAssemblyName::GetName method returns the name part of the assembly name.
                /// </summary>
                /// <param name="lpcwBuffer">Size of the pwszName buffer (on input). Length of the name (on return).</param>
                /// <param name="pwszName">Pointer to the buffer that is to contain the name part of the assembly name.</param>
                /// <returns></returns>
                [PreserveSig]
                int GetName(
                    ref uint lpcwBuffer,
                    [Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pwszName
                );

                /// <summary>
                /// The IAssemblyName::GetVersion method returns the version part of the assembly name.
                /// </summary>
                /// <param name="pdwVersionHi">Pointer to a DWORD that contains the upper 32 bits of the version number.</param>
                /// <param name="pdwVersionLow">Pointer to a DWORD that contain the lower 32 bits of the version number.</param>
                /// <returns></returns>
                [PreserveSig]
                int GetVersion(out uint pdwVersionHi, out uint pdwVersionLow);

                /// <summary>
                /// The IAssemblyName::IsEqual method compares the assembly name to another assembly names.
                /// </summary>
                /// <param name="pName">The assembly name to compare to.</param>
                /// <param name="dwCmpFlags">Indicates which part of the assembly name to use in the comparison.</param>
                /// <returns>S_OK: - The names match according to the comparison criteria.
                /// S_FALSE: - The names do not match.</returns>
                [PreserveSig]
                int IsEqual(IAssemblyName pName, Win32Enums.ASM_CMP_FLAGS dwCmpFlags);

                /// <summary>
                /// The IAssemblyName::Clone method creates a copy of an assembly name.
                /// </summary>
                /// <param name="pName">New instance</param>
                /// <returns></returns>
                [PreserveSig]
                int Clone(out IAssemblyName pName);
            }

            /// <summary>
            /// The IAssemblyEnum interface enumerates the assemblies in the GAC.
            /// </summary>
            [ComImport, Guid("21B8916C-F28E-11D2-A473-00C04F8EF448"),
                InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
            public interface IAssemblyEnum
            {
                /// <summary>
                /// The IAssemblyEnum::GetNextAssembly method enumerates the assemblies in the GAC.
                /// </summary>
                /// <param name="pvReserved">Must be null.</param>
                /// <param name="ppName">Pointer to a memory location that is to receive the interface pointer to the assembly name of the next assembly that is enumerated.</param>
                /// <param name="dwFlags">Must be zero.</param>
                /// <returns></returns>
                [PreserveSig]
                int GetNextAssembly(IntPtr pvReserved, out IAssemblyName ppName, uint dwFlags);

                /// <summary>
                /// Resets this instance.
                /// </summary>
                /// <returns></returns>
                [PreserveSig]
                int Reset();
            }

            /// <summary>
            /// The IInstallReferenceItem interface represents a reference that has been set on an assembly in the GAC. Instances of IInstallReferenceIteam are returned by the IInstallReferenceEnum interface.
            /// </summary>
            [ComImport, Guid("582DAC66-E678-449F-ABA6-6FAAEC8A9394"),
                InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
            public interface IInstallReferenceItem
            {
                /// <summary>
                /// The IInstallReferenceItem::GetReference method returns a FUSION_INSTALL_REFERENCE structure.
                /// </summary>
                /// <param name="ppRefData">A pointer to a FUSION_INSTALL_REFERENCE structure. The memory is allocated by the GetReference method and is freed when IInstallReferenceItem is released. Callers must not hold a reference to this buffer after the IInstallReferenceItem object is released.</param>
                /// <param name="dwFlags">Must be zero.</param>
                /// <param name="pvReserved">Must be null.</param>
                /// <returns></returns>
                [PreserveSig]
                int GetReference(
                    [MarshalAs(UnmanagedType.LPArray)] out Win32Structures.FUSION_INSTALL_REFERENCE[] ppRefData,
                    uint dwFlags,
                    IntPtr pvReserved
                );
            }

            /// <summary>
            /// The IInstallReferenceEnum interface enumerates all references that are set on an assembly in the GAC.
            /// NOTE: References that belong to the assembly are locked for changes while those references are being enumerated.
            /// </summary>
            [ComImport, Guid("56B1A988-7C0C-4AA2-8639-C3EB5A90226F"),
                InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
            public interface IInstallReferenceEnum
            {
                /// <summary>
                /// IInstallReferenceEnum::GetNextInstallReferenceItem returns the next reference information for an assembly.
                /// </summary>
                /// <param name="ppRefItem">Pointer to a memory location that receives the IInstallReferenceItem pointer.</param>
                /// <param name="dwFlags">Must be zero.</param>
                /// <param name="pvReserved">Must be null.</param>
                /// <returns>S_OK: - The next item is returned successfully.
                /// S_FALSE: - No more items.</returns>
                [PreserveSig()]
                int GetNextInstallReferenceItem(
                    out IInstallReferenceItem ppRefItem,
                    uint dwFlags,
                    IntPtr pvReserved
                );
            }
        }
        /// <summary>
        /// String manipulation and generation methods, as well as string array manipulation.
        /// </summary>
        public static class StringUtilities
        {
            /// <summary>
            /// 
            /// </summary>
            public static char[] DefaultQuoteSensitiveChars = new char[] { '\"' };

            private static Random s_random;

            /// <summary>
            /// 
            /// </summary>
            static StringUtilities()
            {
                s_random = new Random(unchecked((int)DateTime.UtcNow.Ticks));
            }

            /// <summary>
            /// Determines whether [is string null or empty with trim] [the specified STR].
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns>
            /// 	<c>true</c> if [is string null or empty with trim] [the specified STR]; otherwise, <c>false</c>.
            /// </returns>
            public static bool IsStringNullOrEmptyWithTrim(string str)
            {
                if (str == null)
                {
                    return true;
                }

                str = str.Trim();
                return str.Length == 0;
            }

            /// <summary>
            /// Joins.
            /// </summary>
            /// <param name="separator">The separator.</param>
            /// <param name="chars">The chars.</param>
            /// <returns></returns>
            public static string Join(string separator, params char[] chars)
            {
                string result = null;

                if (chars != null)
                {
                    int l = chars.Length;
                    for (int i = 0; i < l; i++)
                    {
                        if (i > 0)
                        {
                            result += separator;
                        }
                        result += chars[i];
                    }
                }

                return result;
            }

            /// <summary>
            /// Gets the bytes from string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static byte[] GetBytesFromString(string str)
            {
                // Strings in .NET are always UTF16
                return Encoding.Unicode.GetBytes(str);
            }

            /// <summary>
            /// Gets the string from bytes.
            /// </summary>
            /// <param name="data">The data.</param>
            /// <returns></returns>
            public static string GetStringFromBytes(byte[] data)
            {
                return Encoding.Unicode.GetString(data);
            }

            /// <summary>
            /// Returns a string of length <paramref name="size"/> filled
            /// with random ASCII characters in the range A-Z, a-z. If <paramref name="lowerCase"/>
            /// is <c>true</c>, then the range is only a-z.
            /// </summary>
            /// <param name="size">The size.</param>
            /// <param name="lowerCase">if set to <c>true</c> [lower case].</param>
            /// <returns></returns>
            public static string RandomString(int size, bool lowerCase)
            {
                if (size < 0)
                {
                    throw new ArgumentOutOfRangeException("size", "Size must be positive");
                }
                StringBuilder builder = new StringBuilder(size);
                int low = 65; // 'A'
                int high = 91; // 'Z' + 1
                if (lowerCase)
                {
                    low = 97; // 'a';
                    high = 123; // 'z' + 1
                }
                for (int i = 0; i < size; i++)
                {
                    char ch = Convert.ToChar(s_random.Next(low, high));
                    builder.Append(ch);
                }
                return builder.ToString();
            }

            /// <summary>
            /// Returns a string of length <paramref name="length"/> with
            /// 0's padded to the left, if necessary.
            /// </summary>
            /// <param name="val">The val.</param>
            /// <param name="length">The length.</param>
            /// <returns></returns>
            public static string PadIntegerLeft(int val, int length)
            {
                return PadIntegerLeft(val, length, '0');
            }

            /// <summary>
            /// Pads the integer left.
            /// </summary>
            /// <param name="val">The val.</param>
            /// <param name="length">The length.</param>
            /// <param name="pad">The pad.</param>
            /// <returns></returns>
            public static string PadIntegerLeft(int val, int length, char pad)
            {
                string result = val.ToString();
                while (result.Length < length)
                {
                    result = pad + result;
                }
                return result;
            }

            /// <summary>
            /// Returns a string of length <paramref name="length"/> with
            /// 0's padded to the right, if necessary.
            /// </summary>
            /// <param name="val">The val.</param>
            /// <param name="length">The length.</param>
            /// <returns></returns>
            public static string PadIntegerRight(int val, int length)
            {
                return PadIntegerRight(val, length, '0');
            }

            /// <summary>
            /// Pads the integer right.
            /// </summary>
            /// <param name="val">The val.</param>
            /// <param name="length">The length.</param>
            /// <param name="pad">The pad.</param>
            /// <returns></returns>
            public static string PadIntegerRight(int val, int length, char pad)
            {
                string result = val.ToString();
                while (result.Length < length)
                {
                    result += pad;
                }
                return result;
            }

            /// <summary>
            /// Replace the first occurrence of <paramref name="find"/> (case sensitive) with
            /// <paramref name="replace"/>.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="find">The find.</param>
            /// <param name="replace">The replace.</param>
            /// <returns></returns>
            public static string ReplaceFirst(string str, string find, string replace)
            {
                return ReplaceFirst(str, find, replace, StringComparison.CurrentCulture);
            }

            /// <summary>
            /// Replace the first occurrence of <paramref name="find"/> with
            /// <paramref name="replace"/>.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="find">The find.</param>
            /// <param name="replace">The replace.</param>
            /// <param name="findComparison">The find comparison.</param>
            /// <returns></returns>
            public static string ReplaceFirst(string str, string find, string replace, StringComparison findComparison)
            {
                if (str == null)
                {
                    throw new ArgumentNullException("str");
                }
                else if (string.IsNullOrEmpty(find))
                {
                    throw new ArgumentNullException("find");
                }
                int firstIndex = str.IndexOf(find, findComparison);
                if (firstIndex != -1)
                {
                    if (firstIndex == 0)
                    {
                        str = replace + str.Substring(find.Length);
                    }
                    else if (firstIndex == (str.Length - find.Length))
                    {
                        str = str.Substring(0, firstIndex) + replace;
                    }
                    else
                    {
                        str = str.Substring(0, firstIndex) + replace + str.Substring(firstIndex + find.Length);
                    }
                }
                return str;
            }

            /// <summary>
            /// Splits <paramref name="str"/> based on finding the first location of <paramref name="ch"/>. The first element
            /// is the left portion, and the second element
            /// is the right portion. The character at index <paramref name="index"/>
            /// is not included in either portion.
            /// The return result is never null, and the elements
            /// are never null, so one of the elements may be an empty string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="ch">The character to find.</param>
            /// <returns></returns>
            public static string[] SplitAroundIndexOf(string str, char ch)
            {
                if (str == null)
                {
                    throw new ArgumentNullException("str");
                }
                return SplitAround(str, str.IndexOf(ch));
            }

            /// <summary>
            /// Splits <paramref name="str"/> based on finding the first location of any of the characters from
            /// <paramref name="anyOf"/>. The first element
            /// is the left portion, and the second element
            /// is the right portion. The character at index <paramref name="index"/>
            /// is not included in either portion.
            /// The return result is never null, and the elements
            /// are never null, so one of the elements may be an empty string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="anyOf">Any of.</param>
            /// <returns></returns>
            public static string[] SplitAroundIndexOfAny(string str, params char[] anyOf)
            {
                if (str == null)
                {
                    throw new ArgumentNullException("str");
                }
                return SplitAround(str, str.IndexOfAny(anyOf));
            }

            /// <summary>
            /// Splits <paramref name="str"/> based on finding the last location of <paramref name="ch"/>. The first element
            /// is the left portion, and the second element
            /// is the right portion. The character at index <paramref name="index"/>
            /// is not included in either portion.
            /// The return result is never null, and the elements
            /// are never null, so one of the elements may be an empty string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="ch">The character to find.</param>
            /// <returns></returns>
            public static string[] SplitAroundLastIndexOf(string str, char ch)
            {
                if (str == null)
                {
                    throw new ArgumentNullException("str");
                }
                return SplitAround(str, str.LastIndexOf(ch));
            }

            /// <summary>
            /// Splits <paramref name="str"/> based on finding the last location of any of the charactesr from
            /// <paramref name="anyOf"/>. The first element
            /// is the left portion, and the second element
            /// is the right portion. The character at index <paramref name="index"/>
            /// is not included in either portion.
            /// The return result is never null, and the elements
            /// are never null, so one of the elements may be an empty string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="anyOf">Any of.</param>
            /// <returns></returns>
            public static string[] SplitAroundLastIndexOfAny(string str, params char[] anyOf)
            {
                if (str == null)
                {
                    throw new ArgumentNullException("str");
                }
                return SplitAround(str, str.LastIndexOfAny(anyOf));
            }

            /// <summary>
            /// Splits <paramref name="str"/> based on the index. The first element
            /// is the left portion, and the second element
            /// is the right portion. The character at index <paramref name="index"/>
            /// is not included in either portion.
            /// The return result is never null, and the elements
            /// are never null, so one of the elements may be an empty string.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="index">The index.</param>
            /// <returns></returns>
            public static string[] SplitAround(string str, int index)
            {
                string one, two;
                if (index == -1)
                {
                    one = "";
                    two = str;
                }
                else
                {
                    if (index == 0)
                    {
                        one = "";
                        two = str.Substring(1);
                    }
                    else if (index == str.Length - 1)
                    {
                        one = str.Substring(0, str.Length - 1);
                        two = "";
                    }
                    else
                    {
                        one = str.Substring(0, index);
                        two = str.Substring(index + 1);
                    }
                }

                return new string[] { one, two };
            }

            /// <summary>
            /// Splits the specified pieces.
            /// </summary>
            /// <param name="pieces">The pieces.</param>
            /// <param name="splitChar">The split char.</param>
            /// <param name="indices">The indices.</param>
            /// <returns></returns>
            /*public static string[] Split(string[] pieces, char splitChar, params int[] indices)
            {
                if (pieces == null)
                {
                    throw new ArgumentNullException("pieces");
                }

                if (indices != null && indices.Length == 0)
                {
                    indices = new int[pieces.Length];
                    for (int k = 0; k < indices.Length; k++)
                    {
                        indices[k] = k;
                    }
                }

                // First, we need to sort the indices
                Array.Sort(indices);

                int offset = 0;
                if (indices != null)
                {
                    foreach (int index in indices)
                    {
                        if (index + offset < pieces.Length)
                        {
                            string[] subPieces = pieces[index + offset].Split(splitChar);
                            if (subPieces.Length > 1)
                            {
                                pieces = ArrayUtilities.InsertReplace<string>(pieces, index + offset, subPieces);
                                offset += subPieces.Length - 1;
                            }
                        }
                    }
                }

                return pieces;
            }*/

            /// <summary>
            /// Splits the string based on whitespace, being sensitive to
            /// quotes. Always returns a non-null array, possibly zero-length.
            /// </summary>
            /// <param name="line">The line.</param>
            /// <param name="dividerChars">The divider chars.</param>
            /// <returns></returns>
            /*public static string[] SplitQuoteSensitive(string line, params char[] dividerChars)
            {
                return SplitQuoteSensitive(line, false, dividerChars);
            }*/

            /// <summary>
            /// Splits the string based on whitespace, being sensitive to
            /// quotes. Always returns a non-null array, possibly zero-length.
            /// </summary>
            /// <param name="line">The line.</param>
            /// <param name="retainDivider">if set to <c>true</c> [retain divider].</param>
            /// <param name="dividerChars">The divider chars.</param>
            /// <returns></returns>
           /* public static string[] SplitQuoteSensitive(string line, bool retainDivider, params char[] dividerChars)
            {
                List<string> result = new List<string>();
                if (line != null)
                {
                    if (dividerChars == null || dividerChars.Length == 0)
                    {
                        // no divider chars specified, use the default
                        dividerChars = DefaultQuoteSensitiveChars;
                    }

                    SplitQuoteSensitiveState state = SplitQuoteSensitiveState.InEther;
                    int length = line.Length;
                    char c;
                    StringBuilder sb = new StringBuilder(length);
                    char matchChar = '\0';

                    for (int i = 0; i < length; i++)
                    {
                        c = line[i];
                        if (char.IsWhiteSpace(c))
                        {
                            switch (state)
                            {
                                case SplitQuoteSensitiveState.InPiece:
                                    // the piece has ended
                                    result.Add(sb.ToString());
                                    sb.Length = 0;
                                    state = SplitQuoteSensitiveState.InEther;
                                    break;
                                case SplitQuoteSensitiveState.InDivision:
                                    // whitespace within quotes
                                    sb.Append(c);
                                    break;

                                // ignore:
                                //case SplitQuoteSensitiveState.InEther:
                            }
                        }
                        else if (CharUtilities.IsCharacterOneOf(c, dividerChars) && (matchChar == '\0' || matchChar == c))
                        {
                            switch (state)
                            {
                                case SplitQuoteSensitiveState.InEther:
                                    state = SplitQuoteSensitiveState.InDivision;
                                    matchChar = c;
                                    if (retainDivider)
                                    {
                                        sb.Append(c);
                                    }
                                    break;
                                case SplitQuoteSensitiveState.InPiece:
                                    // quote in the middle of a piece
                                    sb.Append(c);
                                    break;
                                case SplitQuoteSensitiveState.InDivision:
                                    // Finish the piece
                                    result.Add(sb.ToString());
                                    sb.Length = 0;
                                    matchChar = '\0';
                                    state = SplitQuoteSensitiveState.InEther;
                                    if (retainDivider)
                                    {
                                        sb.Append(c);
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            if (state == SplitQuoteSensitiveState.InEther)
                            {
                                state = SplitQuoteSensitiveState.InPiece;
                            }
                            sb.Append(c);
                        }
                    }

                    // See if there is any trailing content
                    switch (state)
                    {
                        case SplitQuoteSensitiveState.InPiece:
                        case SplitQuoteSensitiveState.InDivision:
                            result.Add(sb.ToString());
                            break;
                    }
                }
                return result.ToArray();
            }
            */
            private enum SplitQuoteSensitiveState
            {
                InEther,
                InPiece,
                InDivision
            }

            /// <summary>
            /// Ensures that within <paramref name="str"/> there are no two
            /// consecutive whitespace characters.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static string RemoveConsecutiveWhitespace(string str)
            {
                return ReplaceConsecutiveWhitespace(str, " ");
            }

            /// <summary>
            /// Ensures that within <paramref name="str"/> there are no two
            /// consecutive whitespace characters.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="replacement">The replacement.</param>
            /// <returns></returns>
            public static string ReplaceConsecutiveWhitespace(string str, string replacement)
            {
                return Regex.Replace(str, @"\s+", replacement, RegexOptions.Compiled);
            }

            /// <summary>
            /// Removes all characters passed in from the string.
            /// </summary>
            /// <param name="str"></param>
            /// <param name="chars"></param>
            /// <returns></returns>
            public static string RemoveCharacters(string str, params char[] chars)
            {
                if (chars != null)
                {
                    str = Regex.Replace(str, "[" + new string(chars) + "]+", "");
                }
                return str;
            }

            /// <summary>
            /// Remove all characters that are not in the passed in array
            /// from the string.
            /// </summary>
            /// <param name="str"></param>
            /// <param name="chars"></param>
            /// <returns></returns>
            public static string RemoveCharactersInverse(string str, params char[] chars)
            {
                if (chars != null)
                {
                    str = Regex.Replace(str, "[^" + new string(chars) + "]+", "");
                }
                return str;
            }

            
            /// <summary>
            /// Indexes the of empty piece.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <returns></returns>
            public static int IndexOfEmptyPiece(string[] array)
            {
                return IndexOfEmptyPiece(array, 0);
            }

            /// <summary>
            /// Indexes the of empty piece.
            /// </summary>
            /// <param name="array">The array.</param>
            /// <param name="startIndex">The start index.</param>
            /// <returns></returns>
            public static int IndexOfEmptyPiece(string[] array, int startIndex)
            {
                for (int i = startIndex; i < array.Length; i++)
                {
                    if (string.IsNullOrEmpty(array[i]))
                    {
                        return i;
                    }
                }
                return -1;
            }

            /// <summary>
            /// Splits into two pieces based on the find character.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="find">The find.</param>
            /// <param name="trim">if set to <c>true</c> [trim].</param>
            /// <param name="piece1">The piece1.</param>
            /// <param name="piece2">The piece2.</param>
            public static void FirstSplit(string str, char find, bool trim, out string piece1, out string piece2)
            {
                piece1 = piece2 = null;
                int index = str.IndexOf(find);
                if (index == -1)
                {
                    piece1 = str;
                }
                else
                {
                    if (index == 0)
                    {
                        piece1 = "";
                        piece2 = str.Substring(index + 1);
                    }
                    else if (index == str.Length - 1)
                    {
                        piece1 = str.Substring(0, str.Length - 1);
                        piece2 = "";
                    }
                    else
                    {
                        piece1 = str.Substring(0, index);
                        piece2 = str.Substring(index + 1);
                    }
                    if (trim)
                    {
                        piece1 = piece1.Trim();
                        piece2 = piece2.Trim();
                    }
                }
            }

            /// <summary>
            /// Extracts the first number in the string, discarding the rest.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static int ExtractFirstNumber(string str)
            {
                int result = 0;
                bool foundDigit = false;
                foreach (char c in str)
                {
                    if (char.IsDigit(c))
                    {
                        result = (result * 10) + int.Parse(c.ToString());
                        foundDigit = true;
                    }
                    else if (foundDigit)
                    {
                        break;
                    }
                }
                return result;
            }

            /// <summary>
            /// Searches for all matches to the <paramref name="searches"/>
            /// parameters, and returns the index which is the furthest to
            /// the end of the string. Returns -1 if none of the strings
            /// can be found.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="searches">The searches.</param>
            /// <returns></returns>
            public static int LastIndexOfAny(string str, params string[] searches)
            {
                int result = -1;
                if (searches != null)
                {
                    foreach (string search in searches)
                    {
                        int index = str.LastIndexOf(search);
                        if (index > result)
                        {
                            result = index;
                        }
                    }
                }
                return result;
            }

            

            /// <summary>
            /// Cuts the right.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <param name="resultingLength">Length of the resulting.</param>
            /// <returns></returns>
            public static string CutRight(string str, int resultingLength)
            {
                if (str != null && str.Length > resultingLength)
                {
                    str = str.Substring(0, resultingLength);
                }
                return str;
            }

            

           
            /// <summary>
            /// Trims the newlines.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static string TrimNewlines(string str)
            {
                return TrimNewlinesRight(TrimNewlinesLeft(str));
            }

            /// <summary>
            /// Trims the newlines left.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static string TrimNewlinesLeft(string str)
            {
                if (str != null)
                {
                    int cutLeft = 0;
                    int length = str.Length;
                    char c;

                    for (int i = 0; i < length; i++)
                    {
                        c = str[i];
                        if (c == '\r' || c == '\n')
                        {
                            cutLeft++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (cutLeft > 0)
                    {
                        str = str.Substring(cutLeft);
                    }
                }
                return str;
            }

            /// <summary>
            /// Trims the newlines right.
            /// </summary>
            /// <param name="str">The STR.</param>
            /// <returns></returns>
            public static string TrimNewlinesRight(string str)
            {
                if (str != null)
                {
                    int cutRight = 0;
                    int length = str.Length;
                    char c;

                    for (int i = length - 1; i >= 0; i--)
                    {
                        c = str[i];
                        if (c == '\r' || c == '\n')
                        {
                            cutRight++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (cutRight > 0)
                    {
                        str = str.Substring(0, str.Length - cutRight);
                    }
                }
                return str;
            }

            /// <summary>
            /// Creates the string.
            /// </summary>
            /// <param name="repeat">The repeat.</param>
            /// <param name="count">The count.</param>
            /// <returns></returns>
            public static string CreateString(string repeat, int count)
            {
                if (string.IsNullOrEmpty(repeat) || count <= 0)
                {
                    return repeat;
                }
                StringBuilder sb = new StringBuilder(repeat.Length & count);
                while (count-- > 0)
                {
                    sb.Append(repeat);
                }
                return sb.ToString();
            }

            /// <summary>
            /// Joins the specified integers.
            /// </summary>
            /// <param name="ids">The ids.</param>
            /// <returns></returns>
            public static string Join(params int[] ids)
            {
                if (ids != null)
                {
                    if (ids.Length > 0)
                    {
                        StringBuilder result = new StringBuilder(ids.Length * 3);
                        foreach (int id in ids)
                        {
                            if (result.Length > 0)
                            {
                                result.Append(',');
                            }
                            result.Append(id);
                        }
                        return result.ToString();
                    }
                    return string.Empty;
                }
                return null;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="list"></param>
            /// <returns></returns>
            /*public static string Join(IEnumerable list)
            {
                return Join(",", list);
            }*/

            /// <summary>
            /// 
            /// </summary>
            /// <param name="separator"></param>
            /// <param name="list"></param>
            /// <returns></returns>
            /*public static string Join(string separator, IEnumerable list)
            {
                StringBuilder sb = new StringBuilder(255);
                foreach (object o in list)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(separator);
                    }
                    if (o != null)
                    {
                        sb.Append(o.ToString());
                    }
                    else
                    {
                        sb.Append("null");
                    }
                }
                return sb.ToString();
            }*/

            /// <summary>
            /// 
            /// </summary>
            /// <param name="str"></param>
            /// <param name="find"></param>
            /// <returns></returns>
            public static int CountInstances(string str, char find)
            {
                return CountInstances(str, find.ToString());
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="str"></param>
            /// <param name="find"></param>
            /// <returns></returns>
            public static int CountInstances(string str, string find)
            {
                int result = 0;

                if (!string.IsNullOrEmpty(str) && !string.IsNullOrEmpty(find))
                {
                    int i = str.IndexOf(find);
                    while (i != -1)
                    {
                        result++;
                        i = str.IndexOf(find, i + find.Length);
                    }
                }

                return result;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="str"></param>
            /// <param name="numCharacters"></param>
            /// <returns></returns>
            public static string CutRightCharacters(string str, int numCharacters)
            {
                if (!string.IsNullOrEmpty(str) && numCharacters > 0 && numCharacters <= str.Length)
                {
                    str = str.Substring(0, str.Length - numCharacters);
                }
                return str;
            }

            /// <summary>
            /// Equivalent to new Uri(uri).GetLeftPart(UriPartial.Authority), except it attempts
            /// to be more efficient. Always ends in a trailing slash.
            /// </summary>
            /// <param name="uri"></param>
            /// <returns></returns>
            public static string GetUriAuthority(string uri)
            {
                if (string.IsNullOrEmpty(uri))
                {
                    throw new ArgumentNullException("uri");
                }
                return new Uri(uri).GetLeftPart(UriPartial.Authority) + "/";
            }

            /// <summary>
            /// Returns a non-null string (but it may be an empty string).
            /// </summary>
            /// <param name="uri"></param>
            /// <returns></returns>
            public static string GetPastUriAuthority(string uri)
            {
                return GetPastUriAuthority(uri, false);
            }

            /// <summary>
            /// Returns a non-null string (but it may be an empty string).
            /// </summary>
            /// <param name="uri"></param>
            /// <param name="cutOffQuery"></param>
            /// <returns></returns>
            public static string GetPastUriAuthority(string uri, bool cutOffQuery)
            {
                string authority = GetUriAuthority(uri);
                uri = uri.Substring(authority.Length);

                if (cutOffQuery)
                {
                    int qIndex = uri.IndexOf('?');
                    if (qIndex != -1)
                    {
                        uri = uri.Substring(0, qIndex);
                    }
                }

                return uri;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="output"></param>
            /// <returns></returns>
            public static string GetHumandReadableText(string output)
            {
                if (!string.IsNullOrEmpty(output))
                {
                    output = output.Replace("\r\n", "<br />").Replace("\n", "<br />");
                }
                return output;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="str"></param>
            /// <returns></returns>
            public static string UppercaseWordStarts(string str)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    str = str.ToLower();
                    str = str[0].ToString().ToUpper() + str.Remove(0, 1);
                    // TODO find spaces and uppercase the characters after each space
                }
                return str;
            }
        }
}
